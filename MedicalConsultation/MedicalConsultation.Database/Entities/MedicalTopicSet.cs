﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace MedicalConsultation.Database.Entities
{
    [Table("MedicalTopics")]
    public class MedicalTopicSet
    {
        [Key]
        public int MedicalTopicID { get; set; }

        [Required]
        public string Title { get; set; }
        [Required]
        public string Description { get; set; }

        public DateTime IssueDate { get; set; } = DateTime.UtcNow;
        public int NumberOfVisits { get; set; }

        public string AdminID { get; set; }
        [ForeignKey(nameof(AdminID))]
        public MCIdentityUser Admin { get; set; }
        public string ImageURL { get; set; }
    }
}
