﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace MedicalConsultation.Database.Entities
{
    [Table("Consultations")]
    public class ConsultationSet
    {
        [Key]
        public int ConsultationId { get; set; }


        public DateTime IssuedOn { get; set; } = DateTime.UtcNow;

        [Required]
        public string Subject { get; set; }
        [Required]
        public string Content { get; set; }
        [Required]
        public string HistoricalInformation { get; set; }

        public bool MarkedAsDone { get; set; }

        public ICollection<ConsultationReplySet> ConsultationReplies { get; set; }

        public string PatientId { get; set; }
        [ForeignKey(nameof(PatientId))]
        public MCIdentityUser Patient { get; set; }
    }
}
