﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace MedicalConsultation.Database.Entities
{
    [Table("ConsultationReply")]
    public class ConsultationReplySet
    {
        [Key]
        public int ConsultationReplyID { get; set; }

        public DateTime IssueDate { get; set; } = DateTime.UtcNow;
        [Required]
        public string Subject { get; set; }
        [Required]
        public string Content { get; set; }

        public bool MarkedAsSeen { get; set; }

        public string AdminID { get; set; }
        [ForeignKey(nameof(AdminID))]
        public MCIdentityUser Admin { get; set; }


        public int ConsultationID { get; set; }
        [ForeignKey(nameof(ConsultationID))]
        public ConsultationSet Consultation { get; set; }
    }
}
