﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

using Microsoft.AspNetCore.Identity;

namespace MedicalConsultation.Database.Entities
{
    public class MCIdentityUser : IdentityUser<string>
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string ImageURL { get; set; }

        [DataType(DataType.Date)]
        public DateTime BirthDate { get; set; }

        public string Sex { get; set; }
    }
}
