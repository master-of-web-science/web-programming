﻿using System;
using System.Collections.Generic;
using System.Text;

using MedicalConsultation.Database.Entities;

using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace MedicalConsultation.Database
{
    public class MedicalDbContext : IdentityDbContext<MCIdentityUser, MCIdentityRole, string>
    {
        public MedicalDbContext(DbContextOptions options) : base(options)
        {
        }

        protected MedicalDbContext()
        {
        }

        public DbSet<ConsultationSet> Consultations { get; set; }
        public DbSet<ConsultationReplySet> ConsultationReplies { get; set; }

        public DbSet<MedicalTopicSet> MedicalTopics { get; set; }


        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<ConsultationReplySet>()
                .HasOne(e => e.Consultation)
                .WithMany(e => e.ConsultationReplies)
                .HasForeignKey(e => e.ConsultationID)
                .OnDelete(DeleteBehavior.NoAction);

            builder.Entity<MCIdentityRole>()
                .Property(property => property.Id)
                .ValueGeneratedOnAdd();

            builder.Entity<MCIdentityUser>()
                .Property(property => property.Id)
                .ValueGeneratedOnAdd();

            base.OnModelCreating(builder);
        }
    }
}
