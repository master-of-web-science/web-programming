﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using MedicalConsultation.Database.Entities;

using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;
using Microsoft.Extensions.DependencyInjection;

namespace MedicalConsultation.Database
{
    public static class Seeder
    {

        public static async Task SeedAsync(IServiceProvider serviceProvider)
        {
            var _userManager = serviceProvider.GetService<UserManager<MCIdentityUser>>();
            var _roleManager = serviceProvider.GetService<RoleManager<MCIdentityRole>>();

            // Seeding roles. 

            foreach (string roleItem in new string[] { "Administrator", "Patient" })
            {
                if (await _roleManager.FindByNameAsync(roleItem) is null)
                {
                    var identityResult = await _roleManager.CreateAsync(new MCIdentityRole()
                    {
                        Name = roleItem
                    });

                    if (!identityResult.Succeeded)
                    {
                        throw new Exception("Could not create the roles! Check your seeder!");
                    }
                }
            }


            // Seeding users
            var user = new MCIdentityUser()
            {
                FirstName = "Admin",
                LastName = "Admino",
                UserName = "admin",
                Email = "admin@gmail.com",
                EmailConfirmed = true,
                PhoneNumberConfirmed = true,
                PhoneNumber = "0999999999"
            };

            if (await _userManager.FindByNameAsync(user.UserName) is null)
            {
                var identityResult = await _userManager.CreateAsync(user, "123");

                if (!identityResult.Succeeded)
                {
                    throw new Exception("Could not create the user! Check your seeder!");
                }

                await _userManager.AddToRoleAsync(user, "Administrator");

            }



        }
    }
}
