﻿using System;
using System.Collections.Generic;
using System.Text;

using MedicalConsultation.Services.Shared.Enum;

namespace MedicalConsultation.Services.Core.MedicalTopic
{
    public class MedicalTopicFormDto
    {
        public ActionOperationType OperationType { get; }
        public MedicalTopicFormDto()
        {

        }
        public MedicalTopicFormDto(ActionOperationType actionOperationType)
        {
            OperationType = actionOperationType;
        }
        public int MedicalTopicID { get; set; }

        public string Title { get; set; }
        public string Description { get; set; }

        public DateTime IssueDate { get; set; }
        public int NumberOfVisits { get; set; }

        public string AdminID { get; set; }
        public string ImageURL { get; set; }
    }
}
