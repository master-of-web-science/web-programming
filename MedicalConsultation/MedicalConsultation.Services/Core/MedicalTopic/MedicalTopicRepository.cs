﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Metadata.Ecma335;
using System.Text;

using MedicalConsultation.Database;
using MedicalConsultation.Database.Entities;
using MedicalConsultation.Services.Core.Topic;
using MedicalConsultation.Services.Shared.Enum;

using Microsoft.EntityFrameworkCore;

namespace MedicalConsultation.Services.Core.MedicalTopic
{
    public class MedicalTopicRepository : IMedicalTopicRepository
    {
        private readonly MedicalDbContext _db;
        public MedicalTopicRepository(MedicalDbContext medicalDbContext)
        {
            _db = medicalDbContext;
        }
        public MedicalTopicDto GetMedicalTopic(int id)
        {
            try
            {
                var data = _db.MedicalTopics
                                   .Include(entity => entity.Admin)
                                   .Where(entity => entity.MedicalTopicID == id)
                                   .Select(entity => new MedicalTopicDto()
                                   {
                                       AdminFullName = $"{entity.Admin.FirstName} {entity.Admin.LastName}",
                                       AdminID = entity.AdminID,
                                       MedicalTopicID = entity.MedicalTopicID,
                                       Description = entity.Description,
                                       IssueDate = entity.IssueDate,
                                       NumberOfVisits = entity.NumberOfVisits,
                                       Title = entity.Title,
                                       ImageURL = entity.ImageURL
                                   })
                                   .FirstOrDefault();

                return data;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public IEnumerable<MedicalTopicDto> GetMedicalTopics()
        {
            try
            {
                var data = _db.MedicalTopics
                                   .Include(mt => mt.Admin)
                                   .Select(entity => new MedicalTopicDto()
                                   {
                                       AdminFullName = $"{entity.Admin.FirstName} {entity.Admin.LastName}",
                                       AdminID = entity.AdminID,
                                       MedicalTopicID = entity.MedicalTopicID,
                                       Description = entity.Description,
                                       IssueDate = entity.IssueDate,
                                       NumberOfVisits = entity.NumberOfVisits,
                                       Title = entity.Title,
                                       ImageURL = entity.ImageURL
                                   });
                

                return data;
            }
            catch (Exception)
            {

                return null;
            }
        }


        public bool Remove(int id)
        {
            try
            {
                var entity = _db.MedicalTopics.SingleOrDefault(entity => entity.MedicalTopicID == id);
                if (entity == null)
                {
                    return false;
                }

                _db.Entry(entity).State = EntityState.Deleted;

                _db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public MedicalTopicDto Action(MedicalTopicFormDto formDto)
        {
            if (formDto is null)
            {
                return null;
            }

            try
            {
                var result = formDto.OperationType == ActionOperationType.Create ?
                                                                CreateEntity(formDto)
                                                                : UpdateEntity(formDto);
                return result;
            }
            catch (Exception)
            {
                return null;
            }
        }


        #region Private create medical entity

        private MedicalTopicDto CreateEntity(MedicalTopicFormDto formDto)
        {

            try
            {
                MedicalTopicSet entity = new MedicalTopicSet()
                {
                    AdminID = formDto.AdminID,
                    Description = formDto.Description,
                    IssueDate = formDto.IssueDate,
                    MedicalTopicID = formDto.MedicalTopicID,
                    NumberOfVisits = formDto.NumberOfVisits,
                    Title = formDto.Title,
                    ImageURL = formDto.ImageURL
                };

                _db.Add(entity);
                _db.SaveChanges();

                var returnedData = _db.MedicalTopics
                    .Include(x => x.Admin)
                    .Where(x => x.MedicalTopicID == entity.MedicalTopicID)
                    .Select(x => new MedicalTopicDto()
                    {
                        AdminID = entity.AdminID,
                        Description = entity.Description,
                        IssueDate = entity.IssueDate,
                        MedicalTopicID = entity.MedicalTopicID,
                        NumberOfVisits = entity.NumberOfVisits,
                        Title = entity.Title,
                        ImageURL =entity.ImageURL
                    }).SingleOrDefault();
                
                return returnedData;

            }
            catch (Exception ex)
            {
                return null;
            }
        }

        #endregion

        #region Private update medical entity

        private MedicalTopicDto UpdateEntity(MedicalTopicFormDto formDto)
        {

            var entity = _db.MedicalTopics
                               .Include(mt => mt.Admin)
                               .Where(entity => entity.MedicalTopicID == formDto.MedicalTopicID)
                               .SingleOrDefault();

            if (entity is null)
            {
                return null;
            }

            entity.AdminID = formDto.AdminID;
            entity.Description = formDto.Description;
            entity.IssueDate = formDto.IssueDate;
            entity.MedicalTopicID = formDto.MedicalTopicID;
            entity.NumberOfVisits = formDto.NumberOfVisits;
            entity.Title = formDto.Title;
            entity.ImageURL = formDto.ImageURL;

            _db.SaveChanges();

            var returnedData = new MedicalTopicDto()
            {
                AdminID = entity.AdminID,
                Description = entity.Description,
                IssueDate = entity.IssueDate,
                MedicalTopicID = entity.MedicalTopicID,
                NumberOfVisits = entity.NumberOfVisits,
                Title = entity.Title,
                ImageURL = entity.ImageURL,
                AdminFullName = $"{entity.Admin.FirstName} {entity.Admin.LastName}"
            };
            return returnedData;
        }

        public void IncreateVisitNumber(int id)
        {
            var mtToEncrease = _db.MedicalTopics.SingleOrDefault(x => x.MedicalTopicID == id);
            mtToEncrease.NumberOfVisits += 1;
            _db.SaveChanges();

        }

        #endregion
    }
}
