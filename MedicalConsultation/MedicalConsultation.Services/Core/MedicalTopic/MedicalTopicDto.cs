﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MedicalConsultation.Services.Core.Topic
{
    public class MedicalTopicDto
    {
        public int MedicalTopicID { get; set; }

        public string Title { get; set; }
        public string Description { get; set; }

        public DateTime IssueDate { get; set; }
        public int NumberOfVisits { get; set; }

        public string AdminID { get; set; }
        public string AdminFullName { get; set; }
        public string ImageURL { get; internal set; }
    }
}
