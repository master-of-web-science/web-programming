﻿using System;
using System.Collections.Generic;
using System.Text;

using MedicalConsultation.Services.Core.Topic;

namespace MedicalConsultation.Services.Core.MedicalTopic
{
    public interface IMedicalTopicRepository
    {
        MedicalTopicDto GetMedicalTopic(int id);
        IEnumerable<MedicalTopicDto> GetMedicalTopics();
        bool Remove(int id);
        MedicalTopicDto Action(MedicalTopicFormDto medicalTopicDto);
        void IncreateVisitNumber(int id);
    }
}
