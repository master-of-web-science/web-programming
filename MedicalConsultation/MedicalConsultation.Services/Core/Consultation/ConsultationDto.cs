﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MedicalConsultation.Services.Core.Consultation
{
    public class ConsultationDto
    {
        public int ConsultationId { get; set; }
        public string Content { get; set; }
        public string HistoricalInformation { get; set; }
        public DateTime IssuedOn { get; set; }
        public bool MarkedAsDone { get; set; }

        public string PatientId { get; set; }
        public string PatientFullName { get; set; }

        public string Subject { get; set; }

        public IEnumerable<ConsultationReplyDto> Replies { get; set; }
    }
}
