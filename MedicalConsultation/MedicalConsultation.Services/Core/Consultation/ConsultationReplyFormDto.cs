﻿using System;
using System.Collections.Generic;
using System.Text;
using MedicalConsultation.Services.Shared.Enum;

namespace MedicalConsultation.Services.Core.Consultation
{
    public class ConsultationReplyFormDto
    {

        public ActionOperationType OperationType { get; }
        public ConsultationReplyFormDto(ActionOperationType actionOperationType)
        {
            OperationType = actionOperationType;
        }
        public string AdminID { get;   set; }
        public string Content { get;   set; }
        public DateTime IssuedDate { get;   set; }
        public int ConsultationID { get;   set; }
        public string Subject { get;   set; }
        public bool MarkedAsSeen { get; set; }
    }
}
