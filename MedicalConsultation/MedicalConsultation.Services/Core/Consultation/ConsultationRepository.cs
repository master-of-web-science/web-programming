﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using MedicalConsultation.Database;
using MedicalConsultation.Database.Entities;
using MedicalConsultation.Services.Shared.Enum;

using Microsoft.EntityFrameworkCore;

namespace MedicalConsultation.Services.Core.Consultation
{
    public class ConsultationRepository : IConsultationRepository
    {
        private readonly MedicalDbContext _db;
        public ConsultationRepository(MedicalDbContext medicalDbContext)
        {
            _db = medicalDbContext;
        }

        public ConsultationReplyDto GetConsultationReply(int id)
        {
            try
            {
                var data = _db.ConsultationReplies
                                   .Where(entity => entity.ConsultationReplyID == id)
                                   .Include(entity => entity.Consultation)
                                   .Include(entity => entity.Admin)
                                   .Select(entity => new ConsultationReplyDto()
                                   {
                                       ConsultationReplyID = entity.ConsultationReplyID,
                                       Content = entity.Content,
                                       IssueDate = entity.IssueDate,
                                       ConsultationID = entity.ConsultationID,
                                       AdminID = entity.AdminID,
                                       Subject = entity.Subject,
                                       MarkedAsSeen = entity.MarkedAsSeen,
                                       AdminFullName = $"{entity.Admin.FirstName} {entity.Admin.LastName}"
                                   })
                                   .FirstOrDefault();

                return data;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public ConsultationDto GetConsultation(int id)
        {
            try
            {
                var data = _db.Consultations
                                   .Where(entity => entity.ConsultationId == id)
                                   .Include(entity => entity.ConsultationReplies)
                                   .ThenInclude(en => en.Admin)
                                   .Include(entity => entity.Patient)
                                   .Select(entity => new ConsultationDto()
                                   {
                                       ConsultationId = entity.ConsultationId,
                                       Content = entity.Content,
                                       IssuedOn = entity.IssuedOn,
                                       MarkedAsDone = entity.MarkedAsDone,
                                       PatientId = entity.PatientId,
                                       Subject = entity.Subject,
                                       PatientFullName = $"{entity.Patient.FirstName} {entity.Patient.LastName}",
                                       Replies = entity.ConsultationReplies.Select(reply => new ConsultationReplyDto()
                                       {
                                           AdminID = reply.AdminID,
                                           AdminFullName = $"{reply.Admin.FirstName} {reply.Admin.LastName}",
                                           ConsultationID = reply.ConsultationID,
                                           ConsultationReplyID = reply.ConsultationReplyID,
                                           Content = reply.Content,
                                           IssueDate = reply.IssueDate,
                                           Subject = reply.Subject
                                       })
                                   })
                                   .FirstOrDefault();

                return data;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public IEnumerable<ConsultationReplyDto> GetConsultationReplies(int consultationId)
        {
            try
            {
                var data = _db.ConsultationReplies
                                    .Where(entity => entity.ConsultationID == consultationId)
                                    .Include(entity => entity.Consultation)
                                    .Include(entity => entity.Admin)
                                    .Select(entity => new ConsultationReplyDto()
                                    {
                                        ConsultationReplyID = entity.ConsultationReplyID,
                                        Content = entity.Content,
                                        IssueDate = entity.IssueDate,
                                        ConsultationID = entity.ConsultationID,
                                        AdminID = entity.AdminID,
                                        Subject = entity.Subject
                                    });


                return data;
            }
            catch (Exception)
            {

                return null;
            }
        }

        public IEnumerable<ConsultationDto> GetConsultationsMarkedAsDone()
        {
            try
            {
                var data = _db.Consultations
                                   .Include(consult => consult.ConsultationReplies)
                                   .Where(consult => consult.MarkedAsDone)
                                   .Select(entity => new ConsultationDto()
                                   {
                                       ConsultationId = entity.ConsultationId,
                                       Content = entity.Content,
                                       IssuedOn = entity.IssuedOn,
                                       MarkedAsDone = entity.MarkedAsDone,
                                       PatientId = entity.PatientId,
                                       Subject = entity.Subject,
                                       PatientFullName = $"{entity.Patient.FirstName} {entity.Patient.LastName}",
                                       Replies = entity.ConsultationReplies.Select(reply => new ConsultationReplyDto()
                                       {
                                           AdminID = reply.AdminID,
                                           ConsultationID = reply.ConsultationID,
                                           ConsultationReplyID = reply.ConsultationReplyID,
                                           Content = reply.Content,
                                           IssueDate = reply.IssueDate,
                                           Subject = reply.Subject
                                       })
                                   });


                return data;
            }
            catch (Exception)
            {

                return null;
            }
        }

        public IEnumerable<ConsultationDto> GetConsultationsWaitingToBeAnswered()
        {
            try
            {
                var data = _db.Consultations
                                   .Include(consult => consult.ConsultationReplies)
                                   .Where(consult => !consult.MarkedAsDone)
                                   .OrderByDescending(consult => consult.IssuedOn)
                                   .Select(entity => new ConsultationDto()
                                   {
                                       ConsultationId = entity.ConsultationId,
                                       Content = entity.Content,
                                       IssuedOn = entity.IssuedOn,
                                       MarkedAsDone = entity.MarkedAsDone,
                                       PatientId = entity.PatientId,
                                       Subject = entity.Subject,
                                       PatientFullName = $"{entity.Patient.FirstName} {entity.Patient.LastName}",
                                       Replies = entity.ConsultationReplies.Select(reply => new ConsultationReplyDto()
                                       {
                                           AdminID = reply.AdminID,
                                           ConsultationID = reply.ConsultationID,
                                           ConsultationReplyID = reply.ConsultationReplyID,
                                           Content = reply.Content,
                                           IssueDate = reply.IssueDate,
                                           Subject = reply.Subject
                                       })
                                   });


                return data;
            }
            catch (Exception)
            {

                return null;
            }
        }
        public bool RemoveReply(int id)
        {
            try
            {
                var entity = _db.ConsultationReplies.SingleOrDefault(entity => entity.ConsultationReplyID == id);
                if (entity == null)
                {
                    return false;
                }

                _db.Entry(entity).State = EntityState.Deleted;

                _db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public bool Remove(int id)
        {
            try
            {
                var entity = _db.Consultations.SingleOrDefault(entity => entity.ConsultationId == id);
                if (entity == null)
                {
                    return false;
                }

                _db.Entry(entity).State = EntityState.Deleted;

                _db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }




        public ConsultationDto Action(ConsultationFormDto formDto)
        {
            if (formDto is null)
            {
                return null;
            }

            try
            {
                var result = formDto.OperationType == ActionOperationType.Create ?
                                                                CreateEntity(formDto)
                                                                : UpdateEntity(formDto);
                return result;
            }
            catch (Exception)
            {
                return null;
            }
        }


        #region Private create medical entity

        private ConsultationDto CreateEntity(ConsultationFormDto formDto)
        {

            try
            {
                ConsultationSet entity = new ConsultationSet()
                {
                    ConsultationId = formDto.ConsultationId,
                    Content = formDto.Content,
                    IssuedOn = formDto.IssuedOn,
                    MarkedAsDone = formDto.MarkedAsDone,
                    PatientId = formDto.PatientId,
                    Subject = formDto.Subject,
                    HistoricalInformation = formDto.HistoricalInformation
                };

                _db.Add(entity);
                _db.SaveChanges();

                return GetConsultation(entity.ConsultationId);

            }
            catch (Exception ex)
            {
                return null;
            }
        }

        #endregion

        #region Private update medical entity

        private ConsultationDto UpdateEntity(ConsultationFormDto formDto)
        {

            var entity = GetConsultation(formDto.ConsultationId);

            if (entity is null)
            {
                return null;
            }

            entity.Content = formDto.Content;
            entity.IssuedOn = formDto.IssuedOn;
            entity.MarkedAsDone = formDto.MarkedAsDone;
            entity.PatientId = formDto.PatientId;
            entity.Subject = formDto.Subject;
            entity.HistoricalInformation = formDto.HistoricalInformation;

            _db.SaveChanges();

            var returnedData = GetConsultation(entity.ConsultationId);
            return returnedData;
        }

        #endregion


        public ConsultationReplyDto Reply(ConsultationReplyFormDto formDto)
        {
            try
            {
                using var transaction = _db.Database.BeginTransaction();

                ConsultationReplySet entity = new ConsultationReplySet()
                {
                    AdminID = formDto.AdminID,
                    Content = formDto.Content,
                    IssueDate = formDto.IssuedDate,
                    ConsultationID = formDto.ConsultationID,
                    Subject = formDto.Subject,
                    MarkedAsSeen = formDto.MarkedAsSeen
                };

                try
                {
                    _db.ConsultationReplies.Add(entity);

                    _db.SaveChanges();

                    var consultationSet = _db.Consultations.Where(x => x.ConsultationId == entity.ConsultationID).SingleOrDefault();
                    if (consultationSet is null)
                    {
                        throw new Exception("NO CONSULTATION");
                    }
                    consultationSet.MarkedAsDone = true;
                    _db.Entry(consultationSet).State = EntityState.Modified;
                    _db.SaveChanges();

                    transaction.Commit();
                }
                catch (Exception)
                {
                }

                return GetConsultationReply(entity.ConsultationReplyID);
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public IEnumerable<ConsultationDto> GetPatientConsultation(string patientId)
        {
            try
            {
                var data = _db.Consultations
                                   .Include(consult => consult.ConsultationReplies)
                                   .ThenInclude(x => x.Admin)
                                   .Where(consult => consult.PatientId == patientId)
                                   .Select(entity => new ConsultationDto()
                                   {
                                       ConsultationId = entity.ConsultationId,
                                       Content = entity.Content,
                                       IssuedOn = entity.IssuedOn,
                                       MarkedAsDone = entity.MarkedAsDone,
                                       PatientId = entity.PatientId,
                                       Subject = entity.Subject,
                                       PatientFullName = $"{entity.Patient.FirstName} {entity.Patient.LastName}",
                                       Replies = entity.ConsultationReplies.Select(reply => new ConsultationReplyDto()
                                       {
                                           AdminID = reply.AdminID,
                                           ConsultationID = reply.ConsultationID,
                                           ConsultationReplyID = reply.ConsultationReplyID,
                                           Content = reply.Content,
                                           IssueDate = reply.IssueDate,
                                           Subject = reply.Subject,
                                           MarkedAsSeen = reply.MarkedAsSeen,
                                           AdminFullName = $"{reply.Admin.FirstName} {reply.Admin.LastName}"
                                       })
                                   });


                return data;
            }
            catch (Exception)
            {

                return null;
            }
        }

        public void MarkConsultationRepliesAsSeen(int id)
        {
            var consultation = _db.Consultations.Include(x=>x.ConsultationReplies).SingleOrDefault(i => i.ConsultationId == id);
            if (consultation is null)
            {
                throw new Exception("NO SUCH CONSULT");
            }

            try
            {


                using var transaction = _db.Database.BeginTransaction();
                try
                {
                    foreach (var item in consultation.ConsultationReplies)
                    {
                        item.MarkedAsSeen = true;
                        _db.Entry(item).State = EntityState.Modified;
                        _db.SaveChanges();
                    }

                    transaction.Commit();
                }
                catch (Exception)
                {
                }

            }
            catch (Exception ex)
            {
            }
        }
    }
}
