﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MedicalConsultation.Services.Core.Consultation
{
    public interface IConsultationRepository
    {
        ConsultationDto GetConsultation(int id);
        IEnumerable<ConsultationDto> GetConsultationsMarkedAsDone();
        IEnumerable<ConsultationDto> GetConsultationsWaitingToBeAnswered();

        bool Remove(int id);
        ConsultationDto Action(ConsultationFormDto formDto);
        IEnumerable<ConsultationDto> GetPatientConsultation(string patientId);

        bool RemoveReply(int id);
        ConsultationReplyDto Reply(ConsultationReplyFormDto formDto);
        ConsultationReplyDto GetConsultationReply(int id);

        IEnumerable<ConsultationReplyDto> GetConsultationReplies(int consultationId);
        void MarkConsultationRepliesAsSeen(int id);
    }
}
