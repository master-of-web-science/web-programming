﻿using System;
using System.Collections.Generic;
using System.Text;

using MedicalConsultation.Services.Shared.Enum;

namespace MedicalConsultation.Services.Core.Consultation
{
    public class ConsultationFormDto
    {
        public ActionOperationType OperationType { get; }
        public ConsultationFormDto(ActionOperationType actionOperationType)
        {
            OperationType = actionOperationType;
        }
        public int ConsultationId { get;   set; }
        public string Content { get;   set; }
        public string HistoricalInformation { get;   set; }
        public DateTime IssuedOn { get;   set; }
        public bool MarkedAsDone { get;   set; }
        public string PatientId { get;   set; }
        public string Subject { get;   set; }


    }
}
