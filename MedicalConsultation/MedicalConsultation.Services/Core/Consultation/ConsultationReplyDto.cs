﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MedicalConsultation.Services.Core.Consultation
{
    public class ConsultationReplyDto
    {
        public int ConsultationReplyID { get; set; }

        public DateTime IssueDate { get; set; } 
        public string Subject { get; set; }
        public string Content { get; set; }

        public bool MarkedAsSeen { get; set; }

        public string AdminID { get; set; }
        public string AdminFullName { get; set; }

        public int ConsultationID { get; set; }
    }
}
