﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MedicalConsultation.Services.Core.Patient
{
    public class AccountDto
    {
        public string UserID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }

        public string PhoneNumber { get; set; }
        public DateTime BirthDate { get; set; }
        public string Sex { get; set; }
        public string Email { get; set; }

        public string Role { get; set; }
    }
}
