﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using MedicalConsultation.Services.Core.Account;

namespace MedicalConsultation.Services.Core.Patient
{
    public interface IAccountRepository
    {
        Task<AccountDto> GetAccount(string id);
        Task<IEnumerable<AccountDto>> GetAccounts();
        Task<bool> Remove(string id);
        Task<AccountDto> Action(AccountFormDto formDto);
        Task<IEnumerable<AccountRoleDto>> GetAccountRoles();

        Task<Tuple<bool, IEnumerable<string>>> CheckUsernameCorrectness(string username);
        Task<Tuple<bool, IEnumerable<string>>> CheckEmailCorrectness(string email);
        Task<Tuple<bool, IEnumerable<string>>> CheckPasswordCorrectness(string password);
    }
}
