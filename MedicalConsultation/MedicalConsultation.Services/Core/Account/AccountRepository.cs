﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Metadata.Ecma335;
using System.Text;
using System.Threading.Tasks;

using MedicalConsultation.Database;
using MedicalConsultation.Database.Entities;
using MedicalConsultation.Services.Core.Account;
using MedicalConsultation.Services.Shared.Enum;

using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;

namespace MedicalConsultation.Services.Core.Patient
{
    public class AccountRepository : IAccountRepository
    {
        private readonly UserManager<MCIdentityUser> _userManager;
        private readonly RoleManager<MCIdentityRole> _roleManager;
        private readonly MedicalDbContext _db;

        public AccountRepository(UserManager<MCIdentityUser> userManager,
            RoleManager<MCIdentityRole> roleManager,
            MedicalDbContext db)
        {
            _userManager = userManager;
            _roleManager = roleManager;
            _db = db;
        }

        public async Task<AccountDto> GetAccount(string id)
        {
            try
            {
                var data = await _userManager.FindByIdAsync(id);
                var accountRole = await _userManager.GetRolesAsync(data);
                var returnedData = new AccountDto()
                {
                    BirthDate = data.BirthDate,
                    Email = data.Email,
                    FirstName = data.FirstName,
                    LastName = data.LastName,
                    PhoneNumber = data.PhoneNumber,
                    Role = accountRole.FirstOrDefault(),
                    Sex = data.Sex,
                    UserID = data.Id,
                    Username = data.UserName
                };

                return returnedData;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public async Task<IEnumerable<AccountRoleDto>> GetAccountRoles()
        {
            try
            {
                var roles = await _roleManager.Roles.ToListAsync();

                var rolesDto = roles.Select(x => new AccountRoleDto()
                {
                    RoleId = x.Id,
                    RoleName = x.Name
                });

                return rolesDto;
            }
            catch (Exception)
            {

                return null;
            }
        }

        public async Task<IEnumerable<AccountDto>> GetAccounts()
        {
            try
            {
                var users = await _userManager.Users.ToListAsync();
                var usersDto = new List<AccountDto>();
                foreach (var user in users)
                {
                    AccountDto accountDto = new AccountDto()
                    {
                        BirthDate = user.BirthDate,
                        Email = user.Email,
                        FirstName = user.FirstName,
                        LastName = user.LastName,
                        PhoneNumber = user.PhoneNumber,
                        Sex = user.Sex,
                        UserID = user.Id,
                        Username = user.UserName
                    };
                    var role = await _userManager.GetRolesAsync(user);
                    accountDto.Role = role.FirstOrDefault();
                    usersDto.Add(accountDto);
                }

                return usersDto;

            }
            catch (Exception)
            {

                return null;
            }
        }

        public async Task<bool> Remove(string id)
        {
            try
            {
                var user = await _userManager.FindByIdAsync(id);
                var logins = await _userManager.GetLoginsAsync(user);
                var rolesForUser = await _userManager.GetRolesAsync(user);

                using (var transaction = _db.Database.BeginTransaction())
                {
                    IdentityResult result = IdentityResult.Success;
                    foreach (var login in logins)
                    {
                        result = await _userManager.RemoveLoginAsync(user, login.LoginProvider, login.ProviderKey);
                        if (result != IdentityResult.Success)
                            break;
                    }
                    if (result == IdentityResult.Success)
                    {
                        foreach (var item in rolesForUser)
                        {
                            result = await _userManager.RemoveFromRoleAsync(user, item);
                            if (result != IdentityResult.Success)
                                break;
                        }
                    }
                    if (result == IdentityResult.Success)
                    {
                        result = await _userManager.DeleteAsync(user);
                        if (result == IdentityResult.Success)
                            transaction.Commit();  
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public async Task<AccountDto> Action(AccountFormDto formDto)
        {
            if (formDto is null)
            {
                return null;
            }

            try
            {
                return await CreateEntity(formDto);
            }
            catch (Exception)
            {
                return null;
            }
        }

        #region Private create account

        private async Task<AccountDto> CreateEntity(AccountFormDto formDto)
        {

            try
            {
                bool doesRoleExit = await _roleManager.RoleExistsAsync(formDto.Role);
                if (!doesRoleExit)
                {
                    return null;
                }

                var user = new MCIdentityUser
                {
                    UserName = formDto.Username,
                    BirthDate = formDto.BirthDate,
                    FirstName = formDto.FirstName,
                    LastName = formDto.LastName,
                    PhoneNumber = formDto.PhoneNumber,
                    Sex = formDto.Sex,
                    EmailConfirmed = true,
                    PhoneNumberConfirmed = true,
                    Email = formDto.Email
                };


                var identityResult = await _userManager.CreateAsync(user, formDto.Password);

                if (identityResult.Succeeded)
                {
                    var result1 = await _userManager.AddToRoleAsync(user, formDto.Role);
                }


                return await GetAccount(user.Id);

            }
            catch (Exception)
            {
                return null;
            }
        }


        #region CHECK USERNAME AND PASSWORD AND EMAIL CORRECTNESS
        public async Task<Tuple<bool, IEnumerable<string>>> CheckUsernameCorrectness(string username)
        {
            var usernameErrors = new List<string>();
            if (string.IsNullOrEmpty(username) || string.IsNullOrWhiteSpace(username))
            {
                usernameErrors.Add("Please insert username.");
            }
            var result = await _userManager.FindByNameAsync(username);
            if (result != null)
            {
                usernameErrors.Add("Username already taken, choose another.");
            }
            return new Tuple<bool, IEnumerable<string>>(!usernameErrors.Any(), usernameErrors);
        }



        public async Task<Tuple<bool, IEnumerable<string>>> CheckEmailCorrectness(string email)
        {
            var emailErrors = new List<string>();
            if (string.IsNullOrEmpty(email) || string.IsNullOrWhiteSpace(email))
            {
                emailErrors.Add("Please insert email.");
            }
            var result = await _userManager.FindByEmailAsync(email);
            if (result != null)
            {
                emailErrors.Add("Email already taken, choose another.");
            }
            return new Tuple<bool, IEnumerable<string>>(!emailErrors.Any(), emailErrors);
        }



        public async Task<Tuple<bool, IEnumerable<string>>> CheckPasswordCorrectness(string password)
        {
            var passwordErrors = new List<string>();
            foreach (var validator in _userManager.PasswordValidators)
            {
                var result = await validator.ValidateAsync(_userManager, null, password);
                if (!result.Succeeded)
                {
                    foreach (var error in result.Errors)
                    {
                        passwordErrors.Add(error.Description);
                    }
                    return new Tuple<bool, IEnumerable<string>>(false, passwordErrors);
                }
            }
            return new Tuple<bool, IEnumerable<string>>(true, passwordErrors);
        }
        #endregion

        #endregion
    }
}
