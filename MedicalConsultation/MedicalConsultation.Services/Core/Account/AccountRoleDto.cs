﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MedicalConsultation.Services.Core.Account
{
    public class AccountRoleDto
    {
        public string RoleId { get; set; }
        public string RoleName { get; set; }
    }
}
