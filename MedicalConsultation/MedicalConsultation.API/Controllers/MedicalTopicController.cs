﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using MedicalConsultation.Services.Core.MedicalTopic;
using MedicalConsultation.Services.Shared.Enum;

using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;

namespace MedicalConsultation.API.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class MedicalTopicController : Controller
    {
        private readonly IMedicalTopicRepository _topicRepository;
        private readonly IHostingEnvironment _hostingEnvironment;
        public MedicalTopicController(IMedicalTopicRepository topicRepo, IHostingEnvironment hostingEnvironment)
        {
            _topicRepository = topicRepo;
            _hostingEnvironment = hostingEnvironment;
        }
        [HttpGet]
        public IActionResult GetTopics()
        {
            return Json(_topicRepository.GetMedicalTopics());
        }



        [HttpGet("{id}")]
        public IActionResult GetDetails(int id)
        {
            return Json(_topicRepository.GetMedicalTopic(id));
        }


        [HttpPost]
        public IActionResult Action([FromBody] MedicalTopicFormDto form)
        {
            var operationType = form.MedicalTopicID == 0 ? ActionOperationType.Create : ActionOperationType.Update;

            var result = _topicRepository.Action(new MedicalTopicFormDto(operationType)
            {
                MedicalTopicID = form.MedicalTopicID,
                AdminID = form.AdminID,
                NumberOfVisits = form.NumberOfVisits,
                Description = form.Description,
                IssueDate = form.IssueDate,
                Title = form.Title
            });
            return Ok(new
            {
                Result = result,
            });
        }


        [HttpDelete("{id}")]
        public IActionResult Remove(int id)
        {
            var result = _topicRepository.Remove(id);

            return Ok(new
            {
                Success = result
            });
        }

    }
}
