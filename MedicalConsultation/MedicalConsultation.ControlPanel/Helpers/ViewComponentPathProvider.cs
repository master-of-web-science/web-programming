﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MedicalConsultation.Helpers
{
    public static  class ViewComponentPathProvider
    {
        //Views/Shared/Components/Sidebar/Default.cshtml
        public static string Sidebar => "~/Views/Shared/Components/Sidebar/Default.cshtml";
        public static string Header => "~/Views/Shared/Components/Header/Default.cshtml";

    }
}
