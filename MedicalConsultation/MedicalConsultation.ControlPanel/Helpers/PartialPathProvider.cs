﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MedicalConsultation.Helpers
{
    public class PartialPathProvider
    {
        public static string AdminLayout => "~/Views/Shared/_Layout.cshtml";
        public static string PatientLayout => "~/Views/Shared/_PatientLayout.cshtml";
        public static string LoginLayout => "~/Views/Shared/_IdentityLayout.cshtml";


        public static string ActionModalPartial => "~/Views/Shared/_ActionModal.cshtml";


        public static string ThemeStylePartial => "~/Views/Shared/_ThemeStyle.cshtml";
        public static string ThemeScriptPartial => "~/Views/Shared/_ThemeScripts.cshtml";
        public static string HeaderPartial => "~/Views/Shared/_Header.cshtml";
        public static string FooterPartial => "~/Views/Shared/_Footer.cshtml";


        public static string MedicalTopicTableRow => "~/Views/MedicalTopic/Partials/Table/_TableRaw.cshtml";
        public static string MedicalTopicForm => "~/Views/MedicalTopic/Partials/_MedicalTopicForm.cshtml";


        public static string AccountTableRow => "~/Views/Account/Partials/Table/_TableRaw.cshtml";
        public static string AccountForm => "~/Views/Account/Partials/_AccountForm.cshtml"; 
        
        public static string ConsultationTableRow => "~/Views/Consultation/Partials/Table/_TableRaw.cshtml";
        public static string PatientConsultTableRow => "~/Views/Consultation/Partials/Table/_PatientConsultTableRaw.cshtml";
        public static string ConsultationForm => "~/Views/Consultation/Partials/_ConsultForm.cshtml";
        public static string ConsultationReplyForm => "~/Views/Consultation/Partials/_ConsultReplyForm.cshtml";
    }
}
