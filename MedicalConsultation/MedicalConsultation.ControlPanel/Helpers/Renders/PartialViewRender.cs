﻿using System;
using System.IO;
using System.Threading.Tasks;

using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Abstractions;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.AspNetCore.Mvc.Razor;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Mvc.ViewEngines;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Microsoft.AspNetCore.Routing;

namespace MedicalConsultation.ControlPanel.Helpers.Renders
{
    public class PartialViewRender : IPartialViewRender
    {
        private IRazorViewEngine RazorViewEngine { get; }

        private ITempDataProvider TempDataProvider { get; }

        private HttpContext HttpContext { get; }


        public PartialViewRender(
          IRazorViewEngine razorViewEngine,
          IHttpContextAccessor httpContextAccessor,
          ITempDataProvider tempDataProvider)
        {
            HttpContext = httpContextAccessor.HttpContext;
            RazorViewEngine = razorViewEngine;
            TempDataProvider = tempDataProvider;
        }

        public async Task<string> GetViewAsTextAsync<TModel>(TModel model,  string partialViewPath)
        {

            ViewEngineResult viewResult = RazorViewEngine.GetView(partialViewPath, partialViewPath, false);

            if (!viewResult.Success || viewResult.View == null)
            {
                throw new ArgumentNullException($"There's no such a view. The full path is {partialViewPath}");
            }

            ViewDataDictionary<TModel> viewData = new ViewDataDictionary<TModel>(new EmptyModelMetadataProvider(), new ModelStateDictionary())
            {
                Model = model
            };

            ActionContext actionContext = new ActionContext(HttpContext, new RouteData(), new ActionDescriptor());
            using StringWriter output = new StringWriter();
            ViewContext viewContext = new ViewContext(actionContext, viewResult.View, viewData, new TempDataDictionary(HttpContext, TempDataProvider), output, new HtmlHelperOptions())
            {
                RouteData = RoutingHttpContextExtensions.GetRouteData(this.HttpContext)
            };


            await viewResult.View.RenderAsync(viewContext);

            return output.ToString();
        }
    }
}
