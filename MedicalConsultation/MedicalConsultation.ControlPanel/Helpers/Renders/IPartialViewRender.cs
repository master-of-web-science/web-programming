﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MedicalConsultation.ControlPanel.Helpers.Renders
{
    public interface IPartialViewRender
    {
        Task<string> GetViewAsTextAsync<TModel>(TModel model,string partialViewPath);

    }
}
