class AjaxRequestHandler {

    //#region Helpers

    static get ajaxDataType() {
        return {
            "Xml": "xml",
            "Html": "html",
            "Script": "script",
            "Json": "json",
            "Text": "text",
            "Unkown": false
        };
    }

    static get ajaxRequestType() {
        return {
            "GET": "GET",
            "POST": "POST",
            "PUT": "PUT",
            "DELETE": "DELETE",
        };
    }

    static get ajaxContentType() {
        return {
            "FORM": "application/x-www-form-urlencoded;charset=UTF-8",
            "JSON": "application/json;charset=utf-8",
            "MULTI": "form/multipart",
            "UNKNOWN": false
        };
    }

    //#endregion

    static select2Obj(text, value) {
        return {
            text: text,
            id: value
        };
    }

    static getSelect2Data(select2Input, mapFunc) {
        $(select2Input).select2({
            placeholder: {
                id: -1,
                text: $(select2Input).data("select2-placeholder")
            },
            allowClear: true,
            width: '100%',
            theme: "bootstrap",
            ajax: {
                url: $(select2Input).data("data-provider-url"),
                type: AjaxRequestHandler.ajaxRequestType.GET,
                dataType: AjaxRequestHandler.ajaxDataType.Json,
                processResults: (data) => {
                    return {
                        results: $.map(data, mapFunc)
                    };
                }
            }
        });
    }

    static getJsonData(requestUrl, requestData, onDoneFunc, onFailFunc, printDoneFailParams = false, alwaysFunc = null) {

        var promise = $.ajax({
            url: requestUrl,
            data: requestData,
            dataType: this.ajaxDataType.Json,
            type: this.ajaxRequestType.GET
        });

        promise.done((data, textStatus, jqXHR) => {
            if (printDoneFailParams) {
                console.log("data value is: ", data);
                console.log("----");
                console.log("textStatus value is: ", textStatus);
                console.log("----");
                console.log("jqXHR value is: ", jqXHR);
            }

            if (onDoneFunc) {
                onDoneFunc(data);
            }
        });

        promise.fail((jqXHR, textStatus, errorThrown) => {
            if (printDoneFailParams) {
                console.log("jqXHR value is: ", jqXHR);
                console.log("----");
                console.log("textStatus value is: ", textStatus);
                console.log("----");
                console.log("errorThrown value is: ", errorThrown);
            }

            if (onFailFunc) {
                onFailFunc(jqXHR);
            }
        });

        promise.always((data, textStatus, jqXHR) => {
            if (alwaysFunc) {
                alwaysFunc(data);
            }
        });
    }

    static getWaitingPartialView(requestUrl, onDoneFunc) {
        this.getPartialView(requestUrl, null, onDoneFunc);
    }

    static getPartialView(requestUrl, requestData, onDoneFunc, onFailFunc, alwaysFunc = null, printDoneFailParams = false) {
        var promise = $.ajax({
            url: requestUrl,
            data: requestData,
            dataType: this.ajaxDataType.Html,
            type: this.ajaxRequestType.GET,
        });

        promise.done((data, textStatus, jqXHR) => {
            if (printDoneFailParams) {
                console.log("data value is: ", data);
                console.log("----");
                console.log("textStatus value is: ", textStatus);
                console.log("----");
                console.log("jqXHR value is: ", jqXHR);
            }

            if (onDoneFunc) {
                onDoneFunc(data);
            }
        });

        promise.fail((jqXHR, textStatus, errorThrown) => {
            if (printDoneFailParams) {
                console.log("jqXHR value is: ", jqXHR);
                console.log("----");
                console.log("textStatus value is: ", textStatus);
                console.log("----");
                console.log("errorThrown value is: ", errorThrown);
            }

            if (onFailFunc) {
                onFailFunc(jqXHR);
            }
        });

        promise.always((data, textStatus, jqXHR) => {
            if (alwaysFunc) {
                alwaysFunc(data);
            }
        });

        return promise;
    }



    static saveRecord(requestUrl, requestData, onDoneFunc, onFailFunc, alwaysFunc = null, printDoneFailParams = false) {
        var savePromise = $.ajax({
            url: requestUrl,
            data: requestData,
            type: this.ajaxRequestType.POST,
            contentType: false,
            processData: false
        });

        savePromise.done((data, textStatus, jqXHR) => {
            if (printDoneFailParams) {
                console.log("data value is: ", data);
                console.log("----");
                console.log("textStatus value is: ", textStatus);
                console.log("----");
                console.log("jqXHR value is: ", jqXHR);
            }

            if (onDoneFunc) {
                onDoneFunc(data);
            }
        });

        savePromise.fail((jqXHR, textStatus, errorThrown) => {
            if (printDoneFailParams) {
                console.log("jqXHR value is: ", jqXHR);
                console.log("----");
                console.log("textStatus value is: ", textStatus);
                console.log("----");
                console.log("errorThrown value is: ", errorThrown);
            }

            if (onFailFunc) {
                onFailFunc(jqXHR.responseJSON);
            }
        });

        savePromise.always((data, textStatus, jqXHR) => {
            if (alwaysFunc) {
                alwaysFunc(data);
            }
        });

        return savePromise;
    }

    static removeRecord(requestUrl, requestData, onDoneFunc, onFailFunc, alwaysFunc = null, printDoneFailParams = false) {
        let removePromise = $.ajax({
            url: requestUrl,
            data: requestData,
            dataType: this.ajaxDataType.JSON,
            type: this.ajaxRequestType.DELETE,
        });


        removePromise.done((data, textStatus, jqXHR) => {
            if (printDoneFailParams) {
                console.log("data value is: ", data);
                console.log("----");
                console.log("textStatus value is: ", textStatus);
                console.log("----");
                console.log("jqXHR value is: ", jqXHR);
            }

            if (onDoneFunc) {
                onDoneFunc(data);
            }
        });

        removePromise.fail((jqXHR, textStatus, errorThrown) => {
            if (printDoneFailParams) {
                console.log("jqXHR value is: ", jqXHR);
                console.log("----");
                console.log("textStatus value is: ", textStatus);
                console.log("----");
                console.log("errorThrown value is: ", errorThrown);
            }

            if (onFailFunc) {
                onFailFunc(jqXHR.responseJSON);
            }
        });

        removePromise.always((data, textStatus, jqXHR) => {
            if (alwaysFunc) {
                alwaysFunc(data);
            }
        });
    }
}

export { AjaxRequestHandler };

class FormHandler {


    static prepareInput(inputId, validators) {
        inputId = inputId.trim().startsWith("#") ? inputId : `#${inputId}`;
        return {
            input: $(inputId),
            id: inputId,
            validators: validators,
            getValue: () => {
                return $(inputId).val();
            },
            setValue: (value) => {
                $(inputId).val(value);
            },
            resetValue: () => {
                $(inputId).val(null);
            }
        }
    }

    static prepareSelect2Input(inputId, validators) {
        inputId = inputId.trim().startsWith("#") ? inputId : `#${inputId}`;
        return {
            input: $(inputId),
            id: inputId,
            validators: validators,
            getValue: () => {
                return $(inputId).val();
            },
            setValue: (value) => {
                $(inputId).val(value);
            },
            resetValue: () => {
                $(inputId).val(null).trigger("change");
            }
        }
    }
}


export { FormHandler };
/**
 * This packages require iziToast library https://github.com/marcelodolza/iziToast/
 * */
class NotificationHandler {

    static showStaticWarning(title, message, notificationId = null) {
        notificationId = notificationId.startsWith("#") ? notificationId.replace('#', '') : notificationId;
        iziToast.warning({
            id: notificationId,
            timeout: 0,
            title: title,
            message: message
        });

        let notifyId = this.correctId(notificationId);
        return {
            NotificationElement: $(`${notifyId}`),
            NotificationId: notifyId,
            closeNotification: () => {
                if (!$(notifyId).length) {
                    return;
                }
                this.closeNotification(notifyId);
            }
        };
    }

    static closeNotification(notificationId) {
        notificationId = this.correctId(notificationId);
        $(`${notificationId} > .iziToast-close`).trigger("click");
    }

    static showWarningFor(title, message, timeout = 5000) {
        iziToast.warning({
            timeout: timeout,
            title: title,
            message: message
        });
    }
    static showErrorFor(title, message, timeout = 5000) {
        iziToast.error({
            timeout: timeout,
            title: title,
            message: message
        });
    }
    static showSuccessFor(title, message, timeout = 5000) {
        iziToast.success({
            timeout: timeout,
            title: title,
            message: message
        });
    }
    static correctId(id) {
        return id.startsWith("#") ? id : `#${id}`
    }



    static get swalIcons() {
        return {
            "WARNING": "warning",
            "SUCCESS": "success",
            "INFORMATION": "info",
            "ERROR": "error"
        };
    }

    //#region Private SWAL helper methods

    static getConfirmSwal(title_text, body_text, confirm_btn_text, cancel_btn_text, support_rtl = false) {
        return swal({
            title: title_text,
            text: body_text,
            icon: this.swalIcons.WARNING,
            closeOnEsc: false,
            reverseButtons: support_rtl,
            buttons: {
                confirm: {
                    text: confirm_btn_text,
                    value: {
                        confirmed: true,
                        canceled: false,
                    },
                    visible: true,
                    className: "",
                    closeModal: true
                },
                cancel: {
                    text: cancel_btn_text,
                    value: {
                        confirmed: false,
                        canceled: true,
                    },
                    visible: true,
                    className: "",
                    closeModal: true,
                }

            },
        });
    }
    static getDangerConfirmSwal(titleText, bodyText, confirmBtnText, cancelBtnText, supportRTL = false) {
        return swal({
            title: titleText,
            text: bodyText,
            icon: this.swalIcons.ERROR,
            closeOnEsc: false,
            reverseButtons: supportRTL,
            buttons: {
                cancel: {
                    text: cancelBtnText,
                    value: {
                        confirmed: false,
                        canceled: true,
                    },
                    visible: true,
                    className: "",
                    closeModal: true,
                },
                confirm: {
                    text: confirmBtnText,
                    value: {
                        confirmed: true,
                        canceled: false,
                    },
                    visible: true,
                    className: "",
                    closeModal: true
                },

            },
        });
    }
    static showSuccessSwal(title_text, body_text, ok_btn_text, support_rtl = false) {
        return swal({
            title: title_text,
            body: body_text,
            icon: this.swalIcons.SUCCESS,
            closeOnEsc: false,
            reverseButtons: support_rtl,
            buttons: {
                ok: {
                    text: ok_btn_text,
                    value: {
                        success: true,
                    },
                    visible: true,
                    className: "",
                    closeModal: true,
                }
            }
        });
    }

}


export { NotificationHandler };
class SharedFunctions {
    static get actionClasses() {
        return {
            "removeRecord": ".remove-record",
            "removeTranslationRecord": ".remove-translation-record",
            "editRecord": ".edit-record",
            "addReply": ".add-reply",
            "editTranslationRecord": ".edit-translation-record",

            "removeFileRecord": ".remove-file-record"
        };
    }

    static get actionModalElements() {
        return {
            "createButton": "#createButton",
            "actionModal": "#actionModal",
            "actionModalBody": "#actionModalBody",
            "closeModalButton": "#closeModalButton",
        }
    }
    static get dataMessages() {
        return {
            "notAllowedTitle": "not-allowed-title",
            "notAllowedMsg": "not-allowed-msg",
            "confirmDeleteTitle": "confirm-title-msg",
            "confirmDeleteMessage": "confirm-message-msg",
            "confirmUpdateTitle": "confirm-update-title-msg",
            "confirmUpdateMessage": "confirm-update-message-msg",
            "confirmOkButtonText": "confirm-ok-btn-msg",
            "confirmCancelButtonText": "confirm-cancel-btn-msg"
        };
    }
    static get dataAttributes() {
        return {
            "actionUrl": "action-url",
            "getUrl": "get-url",
            "getByIdUrl": "get-by-id-url",
            "removeUrl": "remove-url",
            "removeTranslationUrl": "remove-translation-url",
            "recordId": "record-id",
            "pageLanguage": "page-language",
            "defaultLanguage": "app-default-language",
            "updateDataTableUrl": "update-datatable-url"
        };
    }
    static getCorrectedId(id) {
        return id.startsWith("#") ? id : `#${id}`;
    }

    static disableModalSubmitButton() {
        $(SharedFunctions.actionModalElements.submitFormModalButton).attr("disabled", "disabled");
    }

    static enableModalSubmitButton() {
        $(SharedFunctions.actionModalElements.submitFormModalButton).attr("disabled", "");
    }

    static preparePageLanguage(languageElement) {
        // languageElement is a jquery element that represent the html tag that a data- attribute of data-page-language
        // which contains the user current language.
        // defaultLanguage is 
        const pageLanguage = languageElement.data(this.dataAttributes.pageLanguage);
        const defaultLangauge = languageElement.data(this.dataAttributes.defaultLanguage);
        const isDefault = pageLanguage.trim().startsWith(defaultLangauge) ? true : false;

        // the return is an object that contains:
        // pageLanguage -> return a string specifing the culture code of the current language
        // isDefault -> return a boolean whether the current language is arabic or not.
        return {
            pageLanguage: pageLanguage,
            pageDefaultLanguage: defaultLangauge,
            isDefault: isDefault,
            testResult: () => {
                console.log(`Page language is (pageLanguage): ${pageLanguage}`);
                console.log(`App default language is (defaultLange): ${defaultLangauge}`);
                console.log(`Is it the default language (isDefault): ${isDefault}`);
            }
        }
    }

    //#region Datatables

    static prepareDataTable(dataTable, language, ordering = true) {
        // The table tag which represents a datatable should contain data attribute called
        // data-remove-url with the delete action method url.
        var configuredDataTable = this.configureDataTableLanguage(dataTable, language, null, ordering);
        return {
            datatable: configuredDataTable,
            updateDataTableUrl: dataTable.data(SharedFunctions.dataAttributes.updateDataTableUrl),

            removeUrl: dataTable.data(SharedFunctions.dataAttributes.removeUrl),
            removeTranslationUrl: dataTable.data(SharedFunctions.dataAttributes.removeTranslationUrl),


            confirmDeleteTitle: dataTable.data(SharedFunctions.dataMessages.confirmDeleteTitle),
            confirmDeleteMessage: dataTable.data(SharedFunctions.dataMessages.confirmDeleteMessage),

            confirmUpdateTitle: dataTable.data(SharedFunctions.dataMessages.confirmUpdateTitle),
            confirmUpdateMessage: dataTable.data(SharedFunctions.dataMessages.confirmUpdateMessage),

            confirmOkButtonText: dataTable.data(SharedFunctions.dataMessages.confirmOkButtonText),
            confirmCancelButtonText: dataTable.data(SharedFunctions.dataMessages.confirmCancelButtonText),

            removeAllRows: () => {
                configuredDataTable.clear().draw(true);
            },
            addRows: (tableBody) => {
                configuredDataTable.clear().draw(true);
                tableBody.forEach((item, index) => {
                    this.addDataTableRecord(item.result, configuredDataTable);
                });
            }
        };
    }

    static configureDataTableLanguage(datatable, language, languageFileLocation = null, ordering = true) {
        datatable.DataTable().destroy();
        languageFileLocation = languageFileLocation == null ? '/lib/datatable/languages' : languageFileLocation;
        return datatable.DataTable({
            responsive: true,
            "language": {
                "url": `${languageFileLocation}/${language}.json`,
            },
            pageLength: 5,
            "ordering": ordering,
            searching: true,
            info: true,
            "autoWidth": true,
            "columnDefs":
                [
                    { "defaultContent": "-", "targets": "_all" },
                ],

        });
    }

    static addDataTableRecord(recordData, dataTable) {
        if (recordData) {
            dataTable.row.add($(recordData)).draw(true);
            $(recordData).fadeIn("slow");
        }
        else {
            console.warn("recordData is falsy.");
        }
    }

    static updateDataTableRecord(recordData, dataTable, tableRow) {
        if (recordData) {
            dataTable.row(tableRow).remove().draw(true);
            this.addDataTableRecord(recordData, dataTable);
        }
        else {
            console.warn("recordData is falsy.");
        }
    }

    static removeDataTableRecord(record, dataTable) {
        // be carefull of using $(this) here with anonymous function () => { ... }
        // because it's a static method $(this) will through an error: Uncaught TypeError: Class constructor SharedFunctions
        // cannot be invoked without 'new'
        record.fadeOut("normal", function () {
            dataTable.row($(this)).remove();
            dataTable.draw(true);
        });
    }

    //#endregion


    static get waitingPartialView() {
        return `
                <div class="please-wait-overlay overlay d-flex justify-content-center align-items-center p-4">
                    <i class="fas fa-2x text-white fa-sync fa-spin"></i>
                </div>
        `;
    }


}


export { SharedFunctions };
class BaseIndex {

    initializeBaseView(datatableId) {
        if (!datatableId) {
            console.error("initializeBaseView requires a datatableId to work");
            return;
        }
        this.pageLanguage = SharedFunctions.preparePageLanguage($("body"));
        this.dataTable = SharedFunctions.prepareDataTable($(SharedFunctions.getCorrectedId(datatableId), false),
            this.pageLanguage.pageLanguage);

        this.actionModal = $(SharedFunctions.actionModalElements.actionModal);
        this.actionModalBody = $(SharedFunctions.actionModalElements.actionModalBody);
        this.actionCreateButton = $(SharedFunctions.actionModalElements.createButton);

        this.getModalBodyInProgress = null;
    }

    bindBaseEvents() {
        $("body").on("click", SharedFunctions.actionModalElements.closeModalButton, (event) => {
            this.actionModal.modal("hide").data("bs.modal", null);

        });
    }

    prepareBaseSelectListItems(input) {
        AjaxRequestHandler.getSelect2Data(input, (item) => {
            return AjaxRequestHandler.select2Obj(`${item.displayName} (${item.neutralCultureCode})`, item.id);
        });
    }

    showWaiting(container, emptyContainer = false) {
        if (emptyContainer) {
            container.empty();
        }
        container.append(SharedFunctions.waitingPartialView);
    }

    hideWaiting() {
        $(".please-wait-overlay").remove();
    }

    showModal(modalToShow, hide = false, showWaitingImage = true) {
        if (showWaitingImage) {
            this.showWaiting(this.actionModalBody, true);
        }

        modalToShow.modal({
            show: !hide,
            keyboard: false,
            backdrop: 'static'
        });
    }

    requestModalBody(requestUrl, requestData, onDoneFunc = null, onFailFunc = null, alwaysFunc = null) {
        if (this.getModalBodyInProgress) {
            this.getModalBodyInProgress.abort();
        }

        this.showModal($(SharedFunctions.actionModalElements.actionModal));

        this.getModalBodyInProgress = AjaxRequestHandler.getPartialView(requestUrl, requestData,
            (onDoneData) => {
                this.actionModalBody.html(onDoneData);
                if (onDoneFunc) {
                    onDoneFunc(onDoneData);
                }
                this.getModalBodyInProgress = null;
            },
            (onFailData) => {
                if (onFailFunc) {
                    onFailFunc(onFailData);
                }
            },
            (alwaysData) => {
                if (alwaysFunc) {
                    alwaysFunc(alwaysData);
                }
                this.hideWaiting();
            });
    }

}


export { BaseIndex };

class ValidationHandler {
    "use strict"

    static setInputState(input, isValid, message) {
        this._setFeedbackCssClasses(input, isValid);
        if (!isValid) {
            this._resetFeedback(input, null);
            this._setFeedbackMessage(input, null, message);
        }
    }
    static checkInputOnChange(input, onChangeFunc) {
        input.on("input", _.debounce((event) => {
            if (onChangeFunc) {
                onChangeFunc(event);
            }
        }, 100));
    }
    static validateInputOnChange(input, validationRules) {
        // see input event -> https://developer.mozilla.org/en-US/docs/Web/API/HTMLElement/input_event
        // see _.debounce function -> https://underscorejs.org/#oop
        input.on("input", _.debounce((event) => {
            ValidationHandler.validateInput(input, validationRules);
        }, 100));
    }

    static validateSelectWithFeedbackOnChange(input, validationRules) {
        input.on("change", _.debounce((event) => {
            ValidationHandler.validateInputWithFeedback(input, validationRules);
        }, 100));
    }

    static validateInputWithFeedbackOnChange(input, validationRules) {
        input.on("input", _.debounce((event) => {
            ValidationHandler.validateInputWithFeedback(input, validationRules);
        }, 100));
    }

    //#region static class variables

    static is_integer_number_validation = "_isIntNumber";
    static is_string_validation = "_isString";
    static is_positive_number_validation = "_isPositiveNumber";
    static is_within_max_length_validation = "_isWithenMaxLength";

    static is_not_empty_validation = "_isNotEmpty";
    static is_real_number_validation = "_isRealNumber";
    static is_selected_validation = "_isSelected";
    static is_file_uploaded_validation = "_isFileUploaded";
    static is_file_uploaded_or_exist_validation = "_isFileUploadedOrExist";


    static _validationClasses = {
        "valid_class": "is-valid",
        "invalid_class": "is-invalid",
        "valid_select_class": "is-select-valid",
        "invalid_select_class": "is-select-invalid",
    }

    static _validationAttributes = {
        "is_not_empty_validation_message": "val-not-empty-msg",
        "is_number_validation_message": "val-is-number-msg",
        "max_length_validation_message": "val-max-length-msg",
        "min_length_validation_message": "val-min-length-msg",
        "min_value_validation_message": "val-min-value-msg",
        "max_value_validation_message": "val-max-value-msg",
        "checked_validation_message": "val-checked-msg",
        "is_real_number_validation_message": "val-is-real-number-msg",
        "is_positive_number_validation_message": "val-is-positive-number-msg",
        "is_selected_validation_message": "val-is-selected-msg",
        "is_file_uploaded_validation_message": "val-is-file-uploaded-msg",
    };

    static validation_message_not_found = "NoValidationMessageWasFound";

    //#endregion

    // Validate input without feedback
    static validateInput(input, validation_rules) {
        return validation_rules.every((validation_rule) => {
            return this[validation_rule](input.val());
        });
    }

    // Validate input with feedback
    static validateInputWithFeedback(input, validation_rules, feedback_input = null, flag = null) {
        return validation_rules.every((validation_rule) => {
            let is_valid = this[validation_rule](input, flag);
            if (!is_valid) {
                this._setFeedbackCssClasses(input, is_valid);
                this._updateFeedback(input, this[`${validation_rule}Message`](input), feedback_input);
            }
            else {
                this._setFeedbackCssClasses(input, is_valid);
                this._resetFeedback(input, feedback_input);
            }
            return is_valid;
        });
    }


    //#region private helper methods

    // set feedback style on input 
    static _setFeedbackCssClasses(input, is_valid) {
        if (is_valid) {
            input.addClass(this._validationClasses.valid_class);
            input.removeClass(this._validationClasses.invalid_class);
        }
        else {
            input.addClass(this._validationClasses.invalid_class);
            input.removeClass(this._validationClasses.valid_class);
        }
    }


    // reset the feedback text then update it with the new message if it was unvalid.
    static _updateFeedback(input, validation_message, feedback_input = null) {
        this._resetFeedback(input, feedback_input);
        this._setFeedbackMessage(input, feedback_input, validation_message);
    }

    static _resetFeedback(input, feedback_input = null) {
        if (feedback_input == null) {
            input.siblings('.invalid-feedback').empty();
        } else {
            feedback_input.empty();
        }
    }

    static _setFeedbackMessage(input, feedback_input, validation_message) {
        if (feedback_input == null) {
            feedback_input = input.siblings('.invalid-feedback');
        }
        if (feedback_input.text().length) {
            feedback_input.html(`${feedback_input.html()}<br/>${validation_message}`)
        }
        else {
            feedback_input.append(validation_message);
        }
    }

    //#endregion 

    //#region Validation Method
    static _isFileUploadedOrExist(input, file_exist) {
        return (input.val() != "" && input.length != 0 && input[0] != undefined && input[0] != null) || file_exist;
    }
    static _isFileUploadedOrExistMessage(input) {
        return this._getValidationMessage(input, this._validationAttributes.is_file_uploaded_validation_message);
    }
    // ----------
    static _isFileUploaded(input) {
        return input.val() != "" && input.length != 0 && input[0] != undefined && input[0] != null;
    }
    static _isFileUploadedMessage(input) {
        return this._getValidationMessage(input, this._validationAttributes.is_file_uploaded_validation_message);
    }
    // ----------

    static _isSelected(input) {
        return input.val() != -1 && input.val() !== null;
    }

    static _isSelectedMessage(input) {
        return this._getValidationMessage(input, this._validationAttributes.is_selected_validation_message);
    }

    // ----------
    static _isNotEmpty(input) {
        return (input.val() !== '' && input.val() !== null && typeof input.val() !== 'undefined') ? true : false;
    }
    static _isNotEmptyMessage(input) {
        return this._getValidationMessage(input, this._validationAttributes.is_not_empty_validation_message);
    }
    // ----------


    static _isPositiveNumber(input) {
        return (this._isRealNumber(input.val()) || this._isIntNumber(input)) && Number(input.val()) >= 0;
    }

    static _isPositiveNumberMessage(input) {
        return this._getValidationMessage(input, this._validationAttributes.is_positive_number_validation_message);
    }

    // ----------

    static _isRealNumber(input) {
        return !!(input.val() % 1) || this._isInt(input) || Math.floor(input.val());
    }

    static _isRealNumberMessage(input) {
        return this._getValidationMessage(input, this._validationAttributes.is_real_number_validation_message);
    }
    // ----------
    static _isIntNumber(input) {
        var number = Math.floor(Number(input.val()));
        return number !== Infinity && String(number) === input.val();
    }

    static _isIntNumberMessage(input) {
        return this._getValidationMessage(input, this._validationAttributes.is_number_validation_message);
    }

    // ----------
    static _isWithinMaxLength(input, max_length) {
        return input.val().val().length >= max_length;
    }

    static _isWithinMaxLengthMessage(input) {
        return this._getValidationMessage(input, this._validationAttributes.max_length_validation_message);
    }

    // ----------
    static _isWithinMinLength(input, min_length) {
        return input.val().length <= min_length;
    }

    static _isWithinMinLengthMessage(input) {
        return this._getValidationMessage(input, this._validationAttributes.min_length_validation_message);
    }

    // ----------
    static _isWithinMinValue(input, min_value = null) {
        if (this._parameterNotNullOrAttributeExist(input, min_value, Object.keys({ min_value }).pop(), 'min') != true) {
            // it will always return false if min_value parameter is null or input did not have a 'min' attribute.
            return false;
        }

        return parseFloat(input.val()) >= min_value;
    }

    static _isWithinMinValueMessage(input) {
        return this._getValidationMessage(input, this._validationAttributes.min_value_validation_message);
    }

    // ----------
    static _isWithinMaxValue(input, max_value = null) {
        if (this._parameterNotNullOrAttributeExist(input, max_value, Object.keys({ max_value }).pop(), 'max') != true) {
            // it will always return false if max_value parameter is null or input did not have a 'max' attribute.
            return false;
        }

        return parseFloat(input.val()) <= max_value;
    }

    static _isWithinMaxValueMessage(input) {
        return this._getValidationMessage(input, this._validationAttributes.max_value_validation_message);
    }

    // ----------
    static _isChecked(input) {
        return input.is(":checked");
    }

    static _isCheckedMessage(input) {
        return this._getValidationMessage(input, this._validationAttributes.checked_validation_message);
    }


    //#region helper private methods

    // To assign set a message
    static _getValidationMessage(input, validation_message_attribute) {
        let validation_message = input.attr(validation_message_attribute);
        return typeof validation_message === 'undefined' ? this.validation_message_not_found : validation_message;
    }

    // To check for paramter or input attribute exist
    static _parameterNotNullOrAttributeExist(input, value, param_name, attr_name) {
        if (value == null) {
            value = parseFloat(input.attr(attr_name));
            if (typeof value === 'undefined') {
                console.warn(`Please provide a value for \'${param_name}\' parameter or an attribute \'${attr_name}\' for \'input\' parameter, otherwise it will always return false.`);
                return false;
            }
        }
        return true;
    }

    //#endregion

    //#endregion
}

export { ValidationHandler };

class PCPShared {
    static get datatables() {
        return {
            "translation": {
                "id": "#translationIndexDataTable",
                "element": $("#translationIndexDataTable")
            },
            "primary": {
                "id": "#indexDataTable",
                "element": $("#indexDataTable")
            }
        };
    }

    static get tabs() {
        return {
            "translation": {
                "id": "#translationsTab",
                "element": $("#translationsTab")
            }
        };
    }

    static get buttons() {
        return {
            "save": {
                "primary": {
                    "id": "#savePrimaryLanguageButton",
                    "element": $("#savePrimaryLanguageButton")
                },
                "translation": {
                    "id": "#saveTranslationButton",
                    "element": $("#saveTranslationButton")
                }
            },
            "reset": {
                "translation": {
                    "id": "#resetTranslationButton",
                    "element": $("#resetTranslationButton")
                }
            },
            "resetForNew": {
                "translation": {
                    "id": "#saveNewTranslationButton",
                    "element": $("#saveNewTranslationButton")
                }
            }
        };
    }
}

export { PCPShared };