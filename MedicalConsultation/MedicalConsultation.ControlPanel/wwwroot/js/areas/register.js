﻿import { AjaxRequestHandler, BaseIndex, ValidationHandler, FormHandler, NotificationHandler, SharedFunctions, PCPShared } from "../../lib/backhelpers/bundle.js";

class RegisterView {
    initView() {
        this.firstName = FormHandler.prepareInput("#firstName", [ValidationHandler.is_not_empty_validation]);
        this.lastName = FormHandler.prepareInput("#lastName", [ValidationHandler.is_not_empty_validation]);
        this.birthDate = FormHandler.prepareInput("#birthDate", [ValidationHandler.is_not_empty_validation]);
        this.phoneNumber = FormHandler.prepareInput("#phoneNumber", [ValidationHandler.is_not_empty_validation]);
        this.username = FormHandler.prepareInput("#username", [ValidationHandler.is_not_empty_validation]);
        this.password = FormHandler.prepareInput("#password", [ValidationHandler.is_not_empty_validation]);
        this.email = FormHandler.prepareInput("#email", [ValidationHandler.is_not_empty_validation]);


        ValidationHandler.validateInputWithFeedbackOnChange(this.firstName.input, this.firstName.validators);
        ValidationHandler.validateInputWithFeedbackOnChange(this.lastName.input, this.lastName.validators);
        ValidationHandler.validateInputWithFeedbackOnChange(this.username.input, this.username.validators);
        ValidationHandler.validateInputWithFeedbackOnChange(this.birthDate.input, this.birthDate.validators);
        ValidationHandler.validateInputWithFeedbackOnChange(this.phoneNumber.input, this.phoneNumber.validators);
        ValidationHandler.validateInputWithFeedbackOnChange(this.password.input, this.password.validators);
        ValidationHandler.validateInputWithFeedbackOnChange(this.email.input, this.email    .validators);

        return this;
    }
    //#region validators

    validateUsername() {

        ValidationHandler.checkInputOnChange(this.username.input, (event) => {
            let requestUrl = $("#registerActionForm").data("check-username-url");
            let oldUsername = $("#registerActionForm").data("old-username-val");
            let requestData = {
                "username": this.username.input.val()
            };
            if (oldUsername !== this.username.input.val()) {
                AjaxRequestHandler.getJsonData(requestUrl, requestData, (data) => {
                    ValidationHandler.setInputState(this.username.input, data.usernameIsValid, data.errors)
                });
            }

            $("#username").keypress((e) => {
                if (48 <= e.which && e.which <= 57) {
                    return true;
                }
                if (65 <= e.which && e.which <= 90) {
                    return true;
                }
                if (97 <= e.which && e.which <= 122) {
                    return true;
                }
                else {
                    return false;
                }
            });
        });
    }

    validateEmail() {
        ValidationHandler.checkInputOnChange(this.email.input, (event) => {
            let requestUrl = $("#registerActionForm").data("check-email-url");
            let requestData = {
                "email": this.email.input.val()
            };
            AjaxRequestHandler.getJsonData(requestUrl, requestData, (data) => {
                ValidationHandler.setInputState(this.email.input, data.emailIsValid, data.errors)
            });
        });
    }

    validatePassword() {
        ValidationHandler.checkInputOnChange(this.password.input, (event) => {
            let requestUrl = $("#registerActionForm").data("check-password-url");
            let requestData = {
                "password": this.password.input.val(),
            };
            AjaxRequestHandler.getJsonData(requestUrl, requestData, (data) => {
                ValidationHandler.setInputState(this.password.input, data.passwordIsValid, data.errors)
            });

        });
    }

    validateForm() {
        let isValid = true;
        isValid &= ValidationHandler.validateInputWithFeedback(this.firstName.input, this.firstName.validators);
        isValid &= ValidationHandler.validateInputWithFeedback(this.lastName.input, this.lastName.validators);
        isValid &= ValidationHandler.validateInputWithFeedback(this.username.input, this.username.validators);
        isValid &= ValidationHandler.validateInputWithFeedback(this.birthDate.input, this.birthDate.validators);
        isValid &= ValidationHandler.validateInputWithFeedback(this.phoneNumber.input, this.phoneNumber.validators);
        isValid &= ValidationHandler.validateInputWithFeedback(this.password.input, this.password.validators);
        isValid &= ValidationHandler.validateInputWithFeedback(this.email.input, this.email.validators);
        return isValid;
    }
    //#endregion
    bindEvents() {
        this.validateUsername();
        this.validatePassword();
        this.validateEmail();
        return this;
    }
}

$(document).ready(() => {
    new RegisterView()
        .initView()
        .bindEvents();
});