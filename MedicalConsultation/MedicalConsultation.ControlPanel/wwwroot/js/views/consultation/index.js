﻿import { AjaxRequestHandler, BaseIndex, ValidationHandler, FormHandler, NotificationHandler, SharedFunctions, PCPShared } from "../../../lib/backhelpers/bundle.js";

class ConsultationIndex extends BaseIndex {

    //#region Initialize Views 

    initializeView() {
        console.log(PCPShared.datatables.primary.id);
        this.initializeBaseView(PCPShared.datatables.primary.id);
        return this;
    }

    initializeModalView() {
        //#region Primary form


        //this.patientConsultationHiddenId = FormHandler.prepareInput("#consultationHiddenID", [ValidationHandler.is_not_empty_validation]);
        //this.patientHiddenId = FormHandler.prepareInput("#patientHiddenID", [ValidationHandler.is_not_empty_validation]);
        //this.subject = FormHandler.prepareInput("#subject", [ValidationHandler.is_not_empty_validation]);
        //this.issuedOn = FormHandler.prepareInput("#issuedOn", [ValidationHandler.is_not_empty_validation]);
        //this.historicalInformation = FormHandler.prepareInput("#historicalInformation", [ValidationHandler.is_not_empty_validation]);
        //this.content = FormHandler.prepareInput("#content", [ValidationHandler.is_not_empty_validation]);
        this.consultationHiddenID = FormHandler.prepareInput("#consultationHiddenID", [ValidationHandler.is_not_empty_validation]);
        this.subject = FormHandler.prepareInput("#subject", [ValidationHandler.is_not_empty_validation]);
        this.content = FormHandler.prepareInput("#content", [ValidationHandler.is_not_empty_validation]);
        this.issuedOn = FormHandler.prepareInput("#issuedOn", [ValidationHandler.is_not_empty_validation]);
        this.adminHiddenID = FormHandler.prepareInput("#adminHiddenID", [ValidationHandler.is_not_empty_validation]);
        this.medicalTopicIsCreated = this.consultationHiddenID.getValue() != 0;


        ValidationHandler.validateInputWithFeedbackOnChange(this.subject.input, this.subject.validators);
        ValidationHandler.validateInputWithFeedbackOnChange(this.issuedOn.input, this.issuedOn.validators);
        ValidationHandler.validateInputWithFeedbackOnChange(this.content.input, this.content.validators);

        //#endregion

        return this;
    }

    //#endregion

    //#region Bind events

    bindModalEvents() {

        $("body").off("click", PCPShared.buttons.save.primary.id)
            .on("click", PCPShared.buttons.save.primary.id, (event) => {
                event.preventDefault();
                if (!this.validateForm()) {
                    return;
                }

                var data = new FormData($("#consultationReplyActionForm")[0]);
                var requestUrl = $("#consultationReplyActionForm").attr("action");
                var isCreateOperation = this.consultationHiddenID.getValue() == 0;
                var savePromise = AjaxRequestHandler.saveRecord(requestUrl, data,
                    (onDoneData) => {
                        NotificationHandler.showSuccessFor(onDoneData.title, onDoneData.message);
                    },
                    (onFailData) => {
                        NotificationHandler.showErrorFor(onFailData.title, onFailData.message);
                    });

                return false;
            });

        return this;
    }


    bindEvents() {
        this.bindBaseEvents();

        $("body").on("click", SharedFunctions.actionModalElements.closeModalButton, (event) => {
            if (this.warningSaveCustomerFirst) {
                this.warningSaveCustomerFirst.closeNotification();
            }
            AjaxRequestHandler.getJsonData(this.dataTable.updateDataTableUrl, null,
                (onDoneData) => {
                    this.dataTable.addRows(onDoneData.data.tableRows);
                    NotificationHandler.showSuccessFor(onDoneData.title, onDoneData.message);
                });
        });

        this.actionCreateButton.click((event) => {
            event.preventDefault();
            var requestUrl = $(event.target).data(SharedFunctions.dataAttributes.getUrl);
            this.requestModalBody(requestUrl, { "id": 0 },
                (modalBodyOnDoneData) => {
                    this.initializeModalView();
                    this.bindModalEvents();
                });

            return false;
        });

        $("body").on("click", SharedFunctions.actionClasses.addReply, (event) => {
            event.preventDefault();
            const requestUrl = $("#indexDataTable").data(SharedFunctions.dataAttributes.getUrl);
            const record = $(event.target).closest("tr");
            const recordId = record.data(SharedFunctions.dataAttributes.recordId);
            console.log(requestUrl);
            console.log(record);
            console.log(recordId);
            this.requestModalBody(requestUrl, { "id": recordId },
                (modalBodyOnDoneData) => {
                    this.initializeModalView();
                    this.bindModalEvents();
                });
            return false;
        });

        return this;
    }

    //#endregion

    //#region Form validations

    validateForm() {
        let isValid = true;
        isValid &= ValidationHandler.validateInputWithFeedback(this.subject.input, this.subject.validators);
        isValid &= ValidationHandler.validateInputWithFeedback(this.issuedOn.input, this.issuedOn.validators);
        isValid &= ValidationHandler.validateInputWithFeedback(this.content.input, this.content.validators);
        return isValid;
    }


    //#endregion
}

$(document).ready(() => {

    new ConsultationIndex()
        .initializeView()
        .bindEvents();
});