﻿import { AjaxRequestHandler, BaseIndex, ValidationHandler, FormHandler, NotificationHandler, SharedFunctions, PCPShared } from "../../../lib/backhelpers/bundle.js";

class MedicalTopicIndex extends BaseIndex {

    //#region Initialize Views 

    initializeView() {
        console.log(PCPShared.datatables.primary.id);
        this.initializeBaseView(PCPShared.datatables.primary.id);
        return this;
    }

    initializeModalView() {
        //#region Primary form

        this.medicalTopicHiddenId = FormHandler.prepareInput("#medicalTopicHiddenID", [ValidationHandler.is_not_empty_validation]);
        this.topicTitle = FormHandler.prepareInput("#topicTitle", [ValidationHandler.is_not_empty_validation]);
        this.issueDate = FormHandler.prepareInput("#issueDate", [ValidationHandler.is_not_empty_validation]);
        this.topicDescription = FormHandler.prepareInput("#topicDescription", [ValidationHandler.is_not_empty_validation]);
        this.medicalTopicIsCreated = this.medicalTopicHiddenId.getValue() != 0;

        this.publisherAdminName = FormHandler.prepareSelect2Input("#publisherAdminName", [ValidationHandler.is_selected_validation]);
        ValidationHandler.validateInputWithFeedbackOnChange(this.topicTitle.input, this.topicTitle.validators);
        ValidationHandler.validateInputWithFeedbackOnChange(this.issueDate.input, this.issueDate.validators);
        ValidationHandler.validateInputWithFeedbackOnChange(this.topicDescription.input, this.topicDescription.validators);
        
        //#endregion

        //#region Translation form


        ValidationHandler.validateSelectWithFeedbackOnChange(this.publisherAdminName.input, this.publisherAdminName.validators);

        //#endregion


        return this;
    }

    //#endregion

    //#region Bind events

    bindModalEvents() {
        FilePond.registerPlugin(
            FilePondPluginFileValidateType,
            FilePondPluginImageExifOrientation,
            FilePondPluginImagePreview,
            FilePondPluginImageCrop,
            FilePondPluginImageResize,
            FilePondPluginImageTransform,
            FilePondPluginImageEdit
        );

        // Select the file input and use 
        // create() to turn it into a pond
        let uploadedFile = FilePond.create(
            document.getElementById("imageURL"),
            {
                labelIdle: `Select a picture`,
                imagePreviewHeight: 170,
                imageCropAspectRatio: '1:1',
                imageResizeTargetWidth: 200,
                imageResizeTargetHeight: 200,
                stylePanelLayout: 'compact circle',
                styleLoadIndicatorPosition: 'center bottom',
                styleProgressIndicatorPosition: 'right bottom',
                styleButtonRemoveItemPosition: 'left bottom',
                styleButtonProcessItemPosition: 'right bottom',
            }
        );

        AjaxRequestHandler.getSelect2Data(this.publisherAdminName.input, (item) => {
            return AjaxRequestHandler.select2Obj(item.username, item.userId);
        });
        var oldFile = $("#medicalTopicActionForm").data("uploaded-file-path");
        console.log(oldFile);
        if (oldFile != "" && oldFile != "/") {
            uploadedFile.addFile(oldFile);
        }
        $("body").off("click", PCPShared.buttons.save.primary.id)
            .on("click", PCPShared.buttons.save.primary.id, (event) => {
                event.preventDefault();
                if (!this.validateForm()) {
                    return;
                }

                //var data = new FormData($("#medicalTopicActionForm")[0]);
                let fileName = "Image";
                let formData = new FormData($("#medicalTopicActionForm")[0]);
                let file = uploadedFile.getFiles();
                if (file.length) {
                    formData.delete(fileName);
                    file.forEach((fileElem) => {
                        formData.append(fileName, fileElem.file);
                    });
                }

                var requestUrl = $("#medicalTopicActionForm").attr("action");
                var isCreateOperation = this.medicalTopicHiddenId.getValue() == 0;
                var savePromise = AjaxRequestHandler.saveRecord(requestUrl, formData,
                    (onDoneData) => {
                        this.medicalTopicHiddenId.setValue(onDoneData.data.data.medicalTopicID);
                        this.medicalTopicIsCreated = this.medicalTopicHiddenId.getValue() != 0;
                        if (isCreateOperation) {
                            SharedFunctions.addDataTableRecord(onDoneData.data.tableRaw, this.dataTable.datatable);
                            this.actionModal.modal("hide").data("bs.modal", null);
                        }
                        else {
                            let recordBeingEdited = $(`tr[data-record-id='${onDoneData.data.data.medicalTopicID}']`);
                            SharedFunctions.updateDataTableRecord(onDoneData.data.tableRaw, this.dataTable.datatable, recordBeingEdited);
                        }
                        NotificationHandler.showSuccessFor(onDoneData.title, onDoneData.message);
                    },
                    (onFailData) => {
                        NotificationHandler.showErrorFor(onFailData.title, onFailData.message);
                    });

                return false;
            });

        return this;
    }


    bindEvents() {
        this.bindBaseEvents();

        $("body").on("click", SharedFunctions.actionModalElements.closeModalButton, (event) => {
            if (this.warningSaveCustomerFirst) {
                this.warningSaveCustomerFirst.closeNotification();
            }
            AjaxRequestHandler.getJsonData(this.dataTable.updateDataTableUrl, null,
                (onDoneData) => {
                    this.dataTable.addRows(onDoneData.data.tableRows);
                    NotificationHandler.showSuccessFor(onDoneData.title, onDoneData.message);
                });
        });

        this.actionCreateButton.click((event) => {
            event.preventDefault();
            var requestUrl = $(event.target).data(SharedFunctions.dataAttributes.getUrl);
            this.requestModalBody(requestUrl, { "id": 0 },
                (modalBodyOnDoneData) => {
                    this.initializeModalView();
                    this.bindModalEvents();
                });

            return false;
        });

        $("body").on("click", SharedFunctions.actionClasses.removeRecord, (event) => {
            event.preventDefault();
            const requestUrl = this.dataTable.removeUrl;
            const record = $(event.target).closest("tr");
            const recordId = record.data(SharedFunctions.dataAttributes.recordId);
            const entityTitle = record.find(".medical-topic-title").text();
            NotificationHandler.getDangerConfirmSwal(
                this.dataTable.confirmDeleteTitle,
                `${this.dataTable.confirmDeleteMessage} ${entityTitle}?`,
                this.dataTable.confirmOkButtonText,
                this.dataTable.confirmCancelButtonText, false).then((operation) => {
                    if (operation.confirmed) {
                        AjaxRequestHandler.removeRecord(requestUrl, { "id": recordId },
                            (onDoneData) => {
                                SharedFunctions.removeDataTableRecord(record, this.dataTable.datatable);
                                NotificationHandler.showSuccessFor(onDoneData.title, onDoneData.message);
                            },
                            (onFailData) => {
                                NotificationHandler.showErrorFor(onFailData.title, onFailData.message);
                            });
                    }
                });
            return false;
        });

        $("body").on("click", SharedFunctions.actionClasses.editRecord, (event) => {
            event.preventDefault();
            const requestUrl = this.actionCreateButton.data(SharedFunctions.dataAttributes.getUrl);
            const record = $(event.target).closest("tr");
            const recordId = record.data(SharedFunctions.dataAttributes.recordId);
            this.requestModalBody(requestUrl, { "id": recordId },
                (modalBodyOnDoneData) => {
                    this.initializeModalView();
                    this.bindModalEvents();
                });
            return false;
        });

        return this;
    }

    //#endregion

    //#region Form validations

    validateForm() {
        let isValid = true;
        isValid &= ValidationHandler.validateInputWithFeedback(this.topicTitle.input, this.topicTitle.validators);
        isValid &= ValidationHandler.validateInputWithFeedback(this.issueDate.input, this.issueDate.validators);
        isValid &= ValidationHandler.validateInputWithFeedback(this.topicDescription.input, this.topicDescription.validators);
        return isValid;
    }


    //#endregion
}

$(document).ready(() => {

    new MedicalTopicIndex()
        .initializeView()
        .bindEvents();
});