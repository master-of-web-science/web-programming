﻿import { AjaxRequestHandler, BaseIndex, ValidationHandler, FormHandler, NotificationHandler, SharedFunctions, PCPShared } from "../../../lib/backhelpers/bundle.js";

class AccountIndex extends BaseIndex {

    //#region Initialize Views 

    initializeView() {
        console.log(PCPShared.datatables.primary.id);
        this.initializeBaseView(PCPShared.datatables.primary.id);
        return this;
    }

    initializeModalView() {
        //#region Primary form

        this.accountHiddenId = FormHandler.prepareInput("#accountHiddenId", [ValidationHandler.is_not_empty_validation]);
        this.firstName = FormHandler.prepareInput("#firstName", [ValidationHandler.is_not_empty_validation]);
        this.lastName = FormHandler.prepareInput("#lastName", [ValidationHandler.is_not_empty_validation]);
        this.birthDate = FormHandler.prepareInput("#birthDate", [ValidationHandler.is_not_empty_validation]);
        this.phoneNumber = FormHandler.prepareInput("#phoneNumber", [ValidationHandler.is_not_empty_validation]);
        this.username = FormHandler.prepareInput("#username", [ValidationHandler.is_not_empty_validation]);
        this.password = FormHandler.prepareInput("#password", [ValidationHandler.is_not_empty_validation]);
        this.email = FormHandler.prepareInput("#email", [ValidationHandler.is_not_empty_validation]);
        this.accountIsCreated = this.accountHiddenId.getValue() != 0;

        this.accountRole = FormHandler.prepareSelect2Input("#accountRole", [ValidationHandler.is_selected_validation]);

        ValidationHandler.validateInputWithFeedbackOnChange(this.firstName.input, this.firstName.validators);
        ValidationHandler.validateInputWithFeedbackOnChange(this.lastName.input, this.lastName.validators);
        ValidationHandler.validateInputWithFeedbackOnChange(this.username.input, this.username.validators);
        ValidationHandler.validateInputWithFeedbackOnChange(this.birthDate.input, this.birthDate.validators);
        ValidationHandler.validateInputWithFeedbackOnChange(this.phoneNumber.input, this.phoneNumber.validators);
        ValidationHandler.validateInputWithFeedbackOnChange(this.password.input, this.password.validators);

        //#endregion

        //#region Translation form


        ValidationHandler.validateSelectWithFeedbackOnChange(this.accountRole.input, this.accountRole.validators);

        //#endregion


        return this;
    }

    //#endregion

    //#region validators

    validateUsername() {

        ValidationHandler.checkInputOnChange(this.username.input, (event) => {
            let requestUrl = $("#accountActionForm").data("check-username-url");
            let oldUsername = $("#accountActionForm").data("old-username-val");
            let requestData = {
                "username": this.username.input.val()
            };
            if (oldUsername !== this.username.input.val()) {
                AjaxRequestHandler.getJsonData(requestUrl, requestData, (data) => {
                    ValidationHandler.setInputState(this.username.input, data.usernameIsValid, data.errors)
                });
            }

            $("#username").keypress((e) => {
                if (48 <= e.which && e.which <= 57) {
                    return true;
                }
                if (65 <= e.which && e.which <= 90) {
                    return true;
                }
                if (97 <= e.which && e.which <= 122) {
                    return true;
                }
                else {
                    return false;
                }
            });
        });
    }

    validateEmail() {
        ValidationHandler.checkInputOnChange(this.email.input, (event) => {
            let requestUrl = $("#accountActionForm").data("check-email-url");
            let requestData = {
                "email": this.email.input.val()
            };
            AjaxRequestHandler.getJsonData(requestUrl, requestData, (data) => {
                ValidationHandler.setInputState(this.email.input, data.emailIsValid, data.errors)
            });
        });
    }

    validatePassword() {
        ValidationHandler.checkInputOnChange(this.password.input, (event) => {
            let requestUrl = $("#accountActionForm").data("check-password-url");
            let requestData = {
                "password": this.password.input.val(),
            };
            AjaxRequestHandler.getJsonData(requestUrl, requestData, (data) => {
                ValidationHandler.setInputState(this.password.input, data.passwordIsValid, data.errors)
            });

        });
    }

    //#endregion


    //#region Bind events

    bindModalEvents() {
        this.validateUsername();
        this.validateEmail();
        this.validatePassword();
        AjaxRequestHandler.getSelect2Data(this.accountRole.input, (item) => {
            return AjaxRequestHandler.select2Obj(item.roleName, item.roleName);
        });

        $("body").off("click", PCPShared.buttons.save.primary.id)
            .on("click", PCPShared.buttons.save.primary.id, (event) => {
                event.preventDefault();
                if (!this.validateForm()) {
                    return;
                }

                var data = new FormData($("#accountActionForm")[0]);
                var requestUrl = $("#accountActionForm").attr("action");
                var isCreateOperation = this.accountHiddenId.getValue() == 0;
                var savePromise = AjaxRequestHandler.saveRecord(requestUrl, data,
                    (onDoneData) => {
                        this.accountHiddenId.setValue(onDoneData.data.data.userID);
                        this.accountIsCreated = this.accountHiddenId.getValue() != 0;
                        if (isCreateOperation) {
                            SharedFunctions.addDataTableRecord(onDoneData.data.tableRaw, this.dataTable.datatable);
                            this.actionModal.modal("hide").data("bs.modal", null);
                        }
                        else {
                            let recordBeingEdited = $(`tr[data-record-id='${onDoneData.data.data.userID}']`);
                            SharedFunctions.updateDataTableRecord(onDoneData.data.tableRaw, this.dataTable.datatable, recordBeingEdited);
                        }
                        NotificationHandler.showSuccessFor(onDoneData.title, onDoneData.message);
                    },
                    (onFailData) => {
                        NotificationHandler.showErrorFor(onFailData.title, onFailData.message);
                    });

                return false;
            });

        return this;
    }


    bindEvents() {
        this.bindBaseEvents();

        $("body").on("click", SharedFunctions.actionModalElements.closeModalButton, (event) => {
            if (this.warningSaveCustomerFirst) {
                this.warningSaveCustomerFirst.closeNotification();
            }
            AjaxRequestHandler.getJsonData(this.dataTable.updateDataTableUrl, null,
                (onDoneData) => {
                    this.dataTable.addRows(onDoneData.data.tableRows);
                    NotificationHandler.showSuccessFor(onDoneData.title, onDoneData.message);
                });
        });

        this.actionCreateButton.click((event) => {
            event.preventDefault();
            var requestUrl = $(event.target).data(SharedFunctions.dataAttributes.getUrl);
            this.requestModalBody(requestUrl, { "id": "" },
                (modalBodyOnDoneData) => {
                    this.initializeModalView();
                    this.bindModalEvents();
                });

            return false;
        });

        $("body").on("click", SharedFunctions.actionClasses.removeRecord, (event) => {
            event.preventDefault();
            const requestUrl = this.dataTable.removeUrl;
            const record = $(event.target).closest("tr");
            const recordId = record.data(SharedFunctions.dataAttributes.recordId);
            const entityTitle = record.find(".user-full-name").text();
            NotificationHandler.getDangerConfirmSwal(
                this.dataTable.confirmDeleteTitle,
                `${this.dataTable.confirmDeleteMessage} ${entityTitle}?`,
                this.dataTable.confirmOkButtonText,
                this.dataTable.confirmCancelButtonText, false).then((operation) => {
                    if (operation.confirmed) {
                        AjaxRequestHandler.removeRecord(requestUrl, { "id": recordId },
                            (onDoneData) => {
                                SharedFunctions.removeDataTableRecord(record, this.dataTable.datatable);
                                NotificationHandler.showSuccessFor(onDoneData.title, onDoneData.message);
                            },
                            (onFailData) => {
                                NotificationHandler.showErrorFor(onFailData.title, onFailData.message);
                            });
                    }
                });
            return false;
        });

        $("body").on("click", SharedFunctions.actionClasses.editRecord, (event) => {
            event.preventDefault();
            const requestUrl = this.actionCreateButton.data(SharedFunctions.dataAttributes.getUrl);
            const record = $(event.target).closest("tr");
            const recordId = record.data(SharedFunctions.dataAttributes.recordId);
            this.requestModalBody(requestUrl, { "id": recordId },
                (modalBodyOnDoneData) => {
                    this.initializeModalView();
                    this.bindModalEvents();
                });
            return false;
        });

        return this;
    }

    //#endregion

    //#region Form validations

    validateForm() {
        let isValid = true;
        isValid &= ValidationHandler.validateInputWithFeedback(this.firstName.input, this.firstName.validators);
        isValid &= ValidationHandler.validateInputWithFeedback(this.lastName.input, this.lastName.validators);
        isValid &= ValidationHandler.validateInputWithFeedback(this.username.input, this.username.validators);
        isValid &= ValidationHandler.validateInputWithFeedback(this.birthDate.input, this.birthDate.validators);
        isValid &= ValidationHandler.validateInputWithFeedback(this.phoneNumber.input, this.phoneNumber.validators);
        isValid &= ValidationHandler.validateInputWithFeedback(this.password.input, this.password.validators);
        isValid &= ValidationHandler.validateInputWithFeedback(this.email.input, this.email.validators);
        ValidationHandler.validateSelectWithFeedbackOnChange(this.accountRole.input, this.accountRole.validators);
        return isValid;
    }


    //#endregion
}

$(document).ready(() => {

    new AccountIndex()
        .initializeView()
        .bindEvents();
});