﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MedicalConsultation.ControlPanel.ViewModels
{
    public class MedicalTopicDetailsViewModel : BaseViewModel
    {
        public MedicalTopicDetailsViewModel(string pageTitle = "Medical Topic",
            string applicationName = "Med. Consult.",
            string pageLanguage = "en", string defaultLangauge = "en")
            : base(pageTitle, applicationName, pageLanguage, defaultLangauge)
        {
        }


        public int MedicalTopicID { get; set; }

        public string Title { get; set; }
        public string Description { get; set; }

        public DateTime IssueDate { get; set; }
        public int NumberOfVisits { get; set; }
        public string ImageURL { get; set; }
        public string AdminID { get; set; }
        public string AdminFullName { get; set; }
    }
}
