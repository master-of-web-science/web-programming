﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MedicalConsultation.ControlPanel.ViewModels
{
    public class AboutViewModel : BaseViewModel
    {
        public bool IsAdmin { get; set; }
        public AboutViewModel(string pageTitle = "About this project", string applicationName = "Med. Consult.", string pageLanguage = "en", string defaultLangauge = "en") : base(pageTitle, applicationName, pageLanguage, defaultLangauge)
        {
        }
    }
}
