﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

namespace MedicalConsultation.ControlPanel.ViewModels
{
    public class HeaderComponentViewModel
    {
        public IEnumerable<Tuple<RegionInfo, CultureInfo>> RegionCultureInfo { get; set; }
        public string RequestScheme { get; set; }
        public string RequestHostHeader { get; set; }
        public string RawUrl { get; set; }
    }
}
