﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace MedicalConsultation.ControlPanel.ViewModels.MedicalTopic
{
    public class MedicalTopicFormViewModel
    {
        public int MedicalTopicID { get; set; }

        public string Title { get; set; }
        public string Description { get; set; }

        public DateTime IssueDate { get; set; } = DateTime.UtcNow;
        public int NumberOfVisits { get; set; }

        public string AdminID { get; set; }
        public string AdminFullName { get; set; }
        public string ImageUrl { get; set; }
        public IFormFile Image { get; set; }

        public IEnumerable<SelectListItem>  Admins { get; set; }
    }
}
