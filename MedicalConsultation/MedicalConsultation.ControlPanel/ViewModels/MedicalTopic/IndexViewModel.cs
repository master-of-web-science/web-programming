﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MedicalConsultation.ControlPanel.ViewModels.Shared;
using MedicalConsultation.Services.Core.Topic;

namespace MedicalConsultation.ControlPanel.ViewModels.MedicalTopic
{
    public class IndexViewModel : BaseViewModel
    {
        public IndexViewModel(string pageTitle, string applicationName ="Med. Conult.", string pageLanguage = "en", string defaultLangauge = "en") : base(pageTitle, applicationName, pageLanguage, defaultLangauge)
        {
        }

        public IEnumerable<MedicalTopicDto> MedicalTopics { get; private set; }
        public bool HasMedicalTopics => MedicalTopicsCount > 0;
        public int MedicalTopicsCount => MedicalTopics.Count();

        public ModalViewModel ModalViewModel { get; set; }

        internal void AddMedicalTopics(IEnumerable<MedicalTopicDto> data)
        {
            MedicalTopics = data ?? new List<MedicalTopicDto>();
        }

        public TableRawViewModel<MedicalTopicDto> GetTableRawViewModel(MedicalTopicDto dto, bool hasPremessions = true, bool canDelete = true, bool canEdit = true)
        {
            return new TableRawViewModel<MedicalTopicDto>(dto, hasPremessions, canDelete, canEdit);
        }

    }
}
