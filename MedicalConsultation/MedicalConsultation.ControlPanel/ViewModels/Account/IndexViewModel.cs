﻿using System.Collections.Generic;
using System.Linq;

using MedicalConsultation.ControlPanel.ViewModels.Shared;
using MedicalConsultation.Services.Core.Patient;

namespace MedicalConsultation.ControlPanel.ViewModels.Account
{
    public class IndexViewModel : BaseViewModel
    {
        public IndexViewModel(string pageTitle, string applicationName ="Med. Conult.", string pageLanguage = "en", string defaultLangauge = "en") : base(pageTitle, applicationName, pageLanguage, defaultLangauge)
        {
        }

        public IEnumerable<AccountDto> Accounts { get; private set; }
        public bool HasAccounts => AccountsCount > 0;
        public int AccountsCount => Accounts.Count();

        public ModalViewModel ModalViewModel { get; set; }

        internal void AddAccounts(IEnumerable<AccountDto> data)
        {
            Accounts = data ?? new List<AccountDto>();
        }

        public TableRawViewModel<AccountDto> GetTableRawViewModel(AccountDto dto, bool hasPremessions = true, bool canDelete = true, bool canEdit = true)
        {
            return new TableRawViewModel<AccountDto>(dto, hasPremessions, canDelete, canEdit);
        }

    }
}
