﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Microsoft.AspNetCore.Mvc.Rendering;

namespace MedicalConsultation.ControlPanel.ViewModels.Account
{
    public class AccountFormViewModel
    {
        public string UserID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }

        public string PhoneNumber { get; set; }
        public DateTime BirthDate { get; set; } = DateTime.UtcNow;
        public string Sex { get; set; }
        public string Email { get; set; }

        public string Role { get; set; }
        public IEnumerable<SelectListItem> Roles { get; set; }
    }
}
