﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using MedicalConsultation.Services.Core.Consultation;
using MedicalConsultation.Services.Core.Patient;
using MedicalConsultation.Services.Core.Topic;

namespace MedicalConsultation.ControlPanel.ViewModels
{
    public class IndexViewModel : BaseViewModel
    {
        public IndexViewModel(string pageTitle, string applicationName,
            string pageLanguage = "en",
            string defaultLangauge = "en") : base(pageTitle, applicationName, pageLanguage, defaultLangauge)
        {
        }
        public bool IsAdmin { get; set; }
        public void SetAccounts(IEnumerable<AccountDto> accountDto)
        {
            accountDto ??= new List<AccountDto>();
            HasAccounts = accountDto.Any();
            AccountCount = accountDto.Count();
        }
        public void SetConsultants(IEnumerable<ConsultationDto> consultationDto)
        {
            consultationDto ??= new List<ConsultationDto>();
            HasConsultants = consultationDto.Any();
            ConsultantCount = consultationDto.Count();
        }
        public void SetMedicalTopics(IEnumerable<MedicalTopicDto> topicDto)
        {
            topicDto ??= new List<MedicalTopicDto>();
            HasMedicalTopics = topicDto.Any();
            TopicCount = topicDto.Count();
        }

        public bool HasMedicalTopics { get; private set; }
        public bool HasAccounts { get; private set; }
        public bool HasConsultants { get; private set; }

        public int TopicCount { get; private set; }
        public int AccountCount { get; private set; }
        public int ConsultantCount { get; private set; }
    }
}
