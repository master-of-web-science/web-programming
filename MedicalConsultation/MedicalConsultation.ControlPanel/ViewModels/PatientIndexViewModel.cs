﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MedicalConsultation.Services.Core.Topic;

namespace MedicalConsultation.ControlPanel.ViewModels
{
    public class PatientIndexViewModel : BaseViewModel
    {
        public IEnumerable<MedicalTopicDto> MedicalTopics { get; set; }
        public PatientIndexViewModel(string pageTitle,
            string applicationName = "Med. Consult.", 
            string pageLanguage = "en", string defaultLangauge = "en") 
            : base(pageTitle, applicationName, pageLanguage, defaultLangauge)
        {
        }

        public bool HasReplyNotSeen { get; set; }
        public void AddMedicalTopics(IEnumerable<MedicalTopicDto> dtos)
        {
            dtos ??= new List<MedicalTopicDto>();
            MedicalTopics = dtos;
        }
    }
}
