﻿namespace MedicalConsultation.ControlPanel.ViewModels
{
    public class ModalViewModel 
    {
        public string ModalTitle { get; set; }
        private string _idPrefix;
        public string IdPrefix
        {
            get => _idPrefix;
            set
            {
                if (value.EndsWith('_'))
                {
                    _idPrefix = value;
                    return;
                }
                _idPrefix = $"{value}_";
            }
        }
    }
}
