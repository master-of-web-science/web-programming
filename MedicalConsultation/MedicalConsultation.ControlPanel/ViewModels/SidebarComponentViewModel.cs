﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MedicalConsultation.ControlPanel.ViewModels
{
    public class SidebarComponentViewModel
    {
        public string Username { get; set; }
        public string FullName { get; set; }
        public string Role { get; set; }
    }
}
