﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using MedicalConsultation.Services.Core.Consultation;

namespace MedicalConsultation.ControlPanel.ViewModels.Consultation
{
    public class MyConsultationDetailsViewModel : BaseViewModel
    {
        public MyConsultationDetailsViewModel(string pageTitle = "Consultation Details", string applicationName = "Med. Consult.",
            string pageLanguage = "en", string defaultLangauge = "en") : base(pageTitle, applicationName, pageLanguage, defaultLangauge)
        {
        }

        public int ConsultationId { get; set; }
        public string PatientId { get; set; }
        public string Content { get; set; }
        public DateTime IssuedOn { get; set; }
        public bool MarkedAsDone { get; set; }
        public string Subject { get; set; }

        public IEnumerable<ConsultationReplyDto> Replies { get; set; }
        public bool IsAdmin { get; set; }
    }
}
