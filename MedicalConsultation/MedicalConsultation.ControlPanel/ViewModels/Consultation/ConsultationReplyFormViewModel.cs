﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MedicalConsultation.ControlPanel.ViewModels.Consultation
{
    public class ConsultationReplyFormViewModel
    {
        public int ConsultationReplyID { get; set; }

        public DateTime IssueDate { get; set; } = DateTime.UtcNow;
        public string Subject { get; set; }
        public string Content { get; set; }

        public bool MarkedAsDone { get; set; }
        public string AdminID { get; set; }

        public int ConsultationID { get; set; }
    }
}
