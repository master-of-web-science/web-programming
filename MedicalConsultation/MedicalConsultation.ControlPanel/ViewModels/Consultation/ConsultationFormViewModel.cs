﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

using Microsoft.AspNetCore.Mvc.Rendering;

namespace MedicalConsultation.ControlPanel.ViewModels.Consultation
{
    public class ConsultationFormViewModel
    {
        public int ConsultationId { get; set; }
        public string Content { get; set; }
        public string HistoricalInformation { get; set; }

        [DataType(DataType.Date)]
        public DateTime IssuedOn { get; set; } = DateTime.Now;
        public bool MarkedAsDone { get; set; }
        public string PatientId { get; set; }
        public string Subject { get; set; }

    }
}
