﻿using System.Collections.Generic;
using System.Linq;

using MedicalConsultation.ControlPanel.ViewModels.Shared;
using MedicalConsultation.Services.Core.Consultation;

namespace MedicalConsultation.ControlPanel.ViewModels.Consultation
{
    public class IndexViewModel : BaseViewModel
    {
        public IndexViewModel(string pageTitle, string applicationName = "Med. Conult.", string pageLanguage = "en", string defaultLangauge = "en") : base(pageTitle, applicationName, pageLanguage, defaultLangauge)
        {
        }

        public IEnumerable<ConsultationDto> Consultations { get; private set; }
        public bool HasConsultation => ConsultationsCount > 0;
        public int ConsultationsCount => Consultations.Count();

        public ModalViewModel ModalViewModel { get; set; }

        internal void AddConsultations(IEnumerable<ConsultationDto> data)
        {
            Consultations = data ?? new List<ConsultationDto>();
        }

        public TableRawViewModel<ConsultationDto> GetTableRawViewModel(ConsultationDto dto, bool hasPremessions = true, bool canDelete = true, bool canEdit = false)
        {
            return new TableRawViewModel<ConsultationDto>(dto, hasPremessions, canDelete, canEdit);
        }

    }
}
