﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MedicalConsultation.ControlPanel.ViewModels.Shared
{
    public class TableRawViewModel<TDto>
    {
        public TableRawViewModel(TDto data, bool hasPremmissions, bool hasPremmissionToDelete, bool hasPremmissionToEdit)
        {
            HasPremmissions = hasPremmissions;
            HasPremmissionToDelete = hasPremmissionToDelete;
            HasPremmissionToEdit = hasPremmissionToEdit;
            RowData = data;
        }
        public TDto RowData { get; }
        public bool HasPremmissions { get; }
        public bool HasPremmissionToDelete { get; }
        public bool HasPremmissionToEdit { get; }
    }
}
