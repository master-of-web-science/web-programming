﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MedicalConsultation.ControlPanel.ViewModels
{
    public class BaseViewModel 
    {
        public string PageTitle { get; private set; }
        public string ApplicationName { get; private set; }
        public string PageLanguage { get; private set; }
        public string DefaultLanguage { get; private set; }
      
        public BaseViewModel(string pageTitle,
                                string applicationName,
                                string pageLanguage = "en",
                                string defaultLangauge = "en")
        {
            PageTitle = pageTitle;
            ApplicationName = applicationName;
            PageLanguage = pageLanguage;
            DefaultLanguage = defaultLangauge;
        }

        public bool ChangePageTitle(string title)
        {
            if (string.IsNullOrEmpty(title))
            {
                return false;
            }
            PageTitle = title;
            return true;
        }

    }
}
