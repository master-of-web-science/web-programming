using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using MedicalConsultation.Database;
using MedicalConsultation.Database.Entities;
using Microsoft.AspNetCore.Mvc.Razor;
using System.Collections.Generic;
using Microsoft.AspNetCore.Localization;
using System.Linq;
using System.Globalization;
using Microsoft.AspNetCore.Identity;
using PaulMiami.AspNetCore.Mvc.Recaptcha;
using MedicalConsultation.Services.Core.MedicalTopic;
using MedicalConsultation.Services.Core.Patient;
using MedicalConsultation.Services.Core.Consultation;
using MedicalConsultation.ControlPanel.Helpers.Renders;

namespace MedicalConsultation.ControlPanel
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<MedicalDbContext>(options =>
                options.UseSqlServer(
                    Configuration.GetValue<string>("ControlPanelSettings:ConnectionStrings:Development"),
                    options => options.MigrationsAssembly("MedicalConsultation.ControlPanel")));
            services.AddIdentity<MCIdentityUser, MCIdentityRole>(options =>
            {
                options.Password.RequireDigit = true;
                options.Password.RequiredUniqueChars = 0;
                options.Password.RequireLowercase = false;
                options.Password.RequireNonAlphanumeric = false;
                options.Password.RequireUppercase = false;
                options.Password.RequiredLength = 3;
                options.SignIn.RequireConfirmedEmail = false;
                options.SignIn.RequireConfirmedPhoneNumber = false;
            })
                .AddEntityFrameworkStores<MedicalDbContext>()
                .AddDefaultUI()
                .AddDefaultTokenProviders();

            services.AddControllersWithViews();
            services.AddLocalization(options => options.ResourcesPath = "Resources");
            services.AddMvc()
                .AddViewLocalization(LanguageViewLocationExpanderFormat.Suffix)
                .AddDataAnnotationsLocalization();

            services.AddRazorPages();
            
            services.AddRecaptcha(new RecaptchaOptions
            {
                SiteKey = "6LeQgf4UAAAAAC6KDMkFWvabTfgxelx2e64GHYs4",
                SecretKey = "6LeQgf4UAAAAAMP8xLnGBRpRaD6I0Aso0U2lymFt"
            });

            var cultures = new List<string>();
            Configuration.GetSection("ControlPanelSettings:SupportedLanguages").Bind(cultures);
            var supportedCultures = cultures.Select(culture => new CultureInfo(culture)).ToList();
            var defaultRequestCulture = Configuration.GetValue<string>("ControlPanelSettings:DefaultRequestCulture");
            
            services.Configure<RequestLocalizationOptions>(options =>
            {
                options.SupportedCultures = supportedCultures;
                options.SupportedUICultures = supportedCultures;
                options.DefaultRequestCulture = new RequestCulture(culture: "en", uiCulture: "en-US");
            });


            services.AddTransient<IMedicalTopicRepository, MedicalTopicRepository>();
            services.AddTransient<IAccountRepository, AccountRepository>();
            services.AddTransient<IConsultationRepository, ConsultationRepository>();

            services.AddTransient<IPartialViewRender, PartialViewRender>();
        }


        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseDatabaseErrorPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/PageNotFound");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }
            app.UseStatusCodePagesWithReExecute("/Home/PageNotFound");
            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
                endpoints.MapRazorPages();
            });


        }
    }
}
