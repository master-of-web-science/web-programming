﻿
using System.Linq;
using System.Threading.Tasks;
using MedicalConsultation.ControlPanel.ViewModels;
using MedicalConsultation.Database.Entities;
using MedicalConsultation.Helpers;

using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace MedicalConsultation.ControlPanel.ViewComponents
{
    public class SidebarViewComponent : ViewComponent
    {

        private readonly UserManager<MCIdentityUser> _userManager;
        public SidebarViewComponent(UserManager<MCIdentityUser> userManager)
        {
            _userManager = userManager;
        }

        public async Task<IViewComponentResult> InvokeAsync()
        {
            var user = await _userManager.FindByNameAsync(User.Identity.Name);
            var userRole = await _userManager.GetRolesAsync(user);
            return View(ViewComponentPathProvider.Sidebar, new SidebarComponentViewModel
            {
                Username = user.UserName,
                FullName = $"{user.FirstName} {user.LastName}",
                Role = userRole.FirstOrDefault()
            }) ;
        }
    }
}
