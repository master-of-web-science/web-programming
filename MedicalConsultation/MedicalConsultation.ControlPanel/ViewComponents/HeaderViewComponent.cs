﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

using MedicalConsultation.ControlPanel.ViewModels;
using MedicalConsultation.Helpers;

using Microsoft.AspNetCore.Http.Features;
using Microsoft.AspNetCore.Mvc;

namespace MedicalConsultation.ControlPanel.ViewComponents
{
    public class HeaderViewComponent : ViewComponent
    {
        public IViewComponentResult Invoke()
        {
            var requestFeature = HttpContext.Features.Get<IHttpRequestFeature>();
            var processedRawUrl = requestFeature.RawTarget.EndsWith('/') || requestFeature.RawTarget.EndsWith('\\') ?
                                    requestFeature.RawTarget : $"{requestFeature.RawTarget}/";
            var forwardPosition = processedRawUrl.Length > 1 ? processedRawUrl.IndexOf('/', 1) : processedRawUrl.IndexOf('/');
            var rawUrl = processedRawUrl.Substring(forwardPosition == -1 ? 0 : forwardPosition);
            return View(ViewComponentPathProvider.Header, new HeaderComponentViewModel()
            {
                RegionCultureInfo = new List<Tuple<RegionInfo, CultureInfo>>
                {
                    new Tuple<RegionInfo, CultureInfo>(new RegionInfo("en-US"), new CultureInfo("en")),
                    new Tuple<RegionInfo, CultureInfo>(new RegionInfo("ar-SA"), new CultureInfo("ar"))
                },
                RawUrl = rawUrl,
                RequestHostHeader = requestFeature.Headers["host"],
                RequestScheme = requestFeature.Scheme
            });
        }
    }
}
