﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

using MedicalConsultation.ControlPanel.Helpers.Renders;
using MedicalConsultation.ControlPanel.ViewModels;
using MedicalConsultation.ControlPanel.ViewModels.MedicalTopic;
using MedicalConsultation.ControlPanel.ViewModels.Shared;
using MedicalConsultation.Helpers;
using MedicalConsultation.Services.Core.MedicalTopic;
using MedicalConsultation.Services.Core.Topic;
using MedicalConsultation.Services.Shared.Enum;

using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace MedicalConsultation.ControlPanel.Controllers
{
    [Authorize(Roles = "Administrator,Patient")]
    public class MedicalTopicController : BaseController
    {
        private readonly IMedicalTopicRepository _medicalTopicRepository;
        private readonly IHostingEnvironment _hostingEnvironment;
        private readonly IPartialViewRender _partialViewRender;
        public MedicalTopicController(IMedicalTopicRepository medicalTopicRepository, 
            IPartialViewRender partialViewRender,
            IHostingEnvironment hostingEnvironment)
        {
            _medicalTopicRepository = medicalTopicRepository;
            _partialViewRender = partialViewRender;
            _hostingEnvironment = hostingEnvironment;
        }

        [AllowAnonymous]
        public IActionResult MedicalTopicDetails(int? id)
        {
            if (id is null || id.Value == 0)
            {
                return RedirectToActionPermanent("Index", "Home");
            }

            _medicalTopicRepository.IncreateVisitNumber(id.Value);
            var mt = _medicalTopicRepository.GetMedicalTopic(id.Value);

            var vm = new MedicalTopicDetailsViewModel()
            {
                AdminFullName = mt.AdminFullName,
                AdminID = mt.AdminID,
                Description = mt.Description,
                IssueDate = mt.IssueDate,
                MedicalTopicID = mt.MedicalTopicID,
                NumberOfVisits = mt.NumberOfVisits,
                Title = mt.Title,
                ImageURL = mt.ImageURL
            };
            return View(vm);
        }

        [Authorize(Roles = "Administrator")]
        public IActionResult Index()
        {
            var vm = new ViewModels.MedicalTopic.IndexViewModel("Medical topics management")
            {
                ModalViewModel = new ViewModels.ModalViewModel()
                {
                    ModalTitle = "Create medical topic"
                }
            };

            vm.AddMedicalTopics(_medicalTopicRepository.GetMedicalTopics());
            return View(vm);
        }

        [HttpGet]
        [Authorize(Roles = "Administrator")]
        public IActionResult GetLoggedInAdmin()
        {
            if (IsNotAjaxRequest())
            {
                return NotFound();
            }
            return Json(new List<object>
            {
                new 
                {
                    Username = User.FindFirst(ClaimTypes.Name).Value,
                    UserId = User.FindFirst(ClaimTypes.NameIdentifier).Value,
                }
            });
        }

        [HttpGet]
        [Authorize(Roles = "Administrator")]
        public IActionResult GetMedicalTopicForm(int id)
        {
            if (IsNotAjaxRequest())
            {
                return NotFound();
            }
            var formVM = new MedicalTopicFormViewModel();

            if (id == 0)
            {
                return PartialView(PartialPathProvider.MedicalTopicForm, formVM);
            }

            var entityResult = _medicalTopicRepository.GetMedicalTopic(id);
            if (entityResult is null)
            {
                return GetFailureJsonResult("Doesn't exist", "There's medical topic with this ID");
            }

            formVM.AdminFullName = entityResult.AdminFullName;
            formVM.AdminID = entityResult.AdminID;
            formVM.Description = entityResult.Description;
            formVM.IssueDate = entityResult.IssueDate;
            formVM.MedicalTopicID = entityResult.MedicalTopicID;
            formVM.NumberOfVisits = entityResult.NumberOfVisits;
            formVM.Title = entityResult.Title;
            formVM.ImageUrl = entityResult.ImageURL;
            formVM.Admins = new List<SelectListItem>()
            {
                new SelectListItem()
                {
                    Text = entityResult.AdminFullName,
                    Value = entityResult.AdminID,
                    Selected = true
                }
            };

            return PartialView(PartialPathProvider.MedicalTopicForm, formVM);
        }


        [HttpGet]
        [Authorize(Roles = "Administrator")]
        public IActionResult GetIndexDatatable()
        {
            if (IsNotAjaxRequest())
            {
                return NotFound();
            }
            var medicalTopics = _medicalTopicRepository.GetMedicalTopics();
            var rawPartialViewModel = medicalTopics.Select(mt => new TableRawViewModel<MedicalTopicDto>(mt, true, true, true));
            try
            {
                return GetSuccessJsonResult("Updating datatable - successed!", "The datatable has been updated successfully with the new data!", new
                {
                    TableRows = rawPartialViewModel.Select(async row => await _partialViewRender.GetViewAsTextAsync(row, PartialPathProvider.MedicalTopicTableRow))
                });
            }
            catch
            {
                return GetFailureJsonResult("Saving medical topic successed but! :(", "We couldn't refresh the table, please refresh the page!");
            }
        }

        private string GetUniqueFileName(string fileName)
        {
            fileName = Path.GetFileName(fileName);
            return Path.GetFileNameWithoutExtension(fileName)
                      + "_"
                      + Guid.NewGuid().ToString().Substring(0, 4)
                      + Path.GetExtension(fileName);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrator")]
        public async Task<IActionResult> Action(MedicalTopicFormViewModel formVM)
        {
            if (IsNotAjaxRequest())
            {
                return NotFound();
            }

            if (!ModelState.IsValid)
            {
                return GetFailureJsonResult("Form is invalid", "Please fix the form error!");
            }
            var actionOperationType = formVM.MedicalTopicID == 0 ? ActionOperationType.Create : ActionOperationType.Update;
            string uniqueFileName = string.Empty;
            if (formVM.Image != null)
            {
                uniqueFileName = GetUniqueFileName(formVM.Image.FileName);
                var uploads = Path.Combine(_hostingEnvironment.WebRootPath, "images/uploads");
                var filePath = Path.Combine(uploads, uniqueFileName);
                formVM.Image.CopyTo(new FileStream(filePath, FileMode.Create));

                //to do : Save uniqueFileName  to your db table   
            }

            var data = new MedicalTopicFormDto(actionOperationType)
            {
                AdminID = formVM.AdminID,
                Description = formVM.Description,
                IssueDate = formVM.IssueDate,
                MedicalTopicID = formVM.MedicalTopicID,
                NumberOfVisits = formVM.NumberOfVisits,
                Title = formVM.Title,
                ImageURL = uniqueFileName
            };

            var result = _medicalTopicRepository.Action(data);

            if (result is null)
            {

                return GetFailureJsonResult("Saving medical topic - failed :(", $"Could not save the medical topic try again.");
            }

            var returnedData = _medicalTopicRepository.GetMedicalTopic(result.MedicalTopicID);
            var rawPartialViewModel = new TableRawViewModel<MedicalTopicDto>(returnedData, true, true, true);

            try
            {
                return GetSuccessJsonResult("Saving medical topic - success! ", $"Your medical topic <b>{data.Title} </b> has been saved successfully!", new
                {
                    TableRaw = await _partialViewRender.GetViewAsTextAsync(rawPartialViewModel, PartialPathProvider.MedicalTopicTableRow),
                    Data = returnedData
                });
            }
            catch
            {
                return GetFailureJsonResult("Saving medical topic successed but! :(", "We couldn't refresh the table, please refresh the page!");
            }
        }


        [HttpDelete]
        [Authorize(Roles = "Administrator")]
        public IActionResult Remove(int id)
        {
            try
            {
                if (IsNotAjaxRequest())
                {
                    return NotFound();
                }
                if (id == 0)
                {
                    return GetFailureJsonResult("Remove medical topic - failed :(", "Your id is null.");
                }
                var removeResult = _medicalTopicRepository.Remove(id);
                if (!removeResult)
                {
                    return GetFailureJsonResult("Remove medical topic - failed :(", "");
                }
            }
            catch (Exception)
            {
                return GetFailureJsonResult("Remove medical topic - failed :(", "Your id is null.");

            }
            return GetSuccessJsonResult("Remove medical topic - successed!", "The medical topic has been deleted successfully!");
        }

    }
}
