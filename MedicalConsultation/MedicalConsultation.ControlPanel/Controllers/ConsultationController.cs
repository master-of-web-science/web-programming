﻿using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

using MedicalConsultation.ControlPanel.Helpers.Renders;
using MedicalConsultation.ControlPanel.ViewModels.Consultation;
using MedicalConsultation.ControlPanel.ViewModels.Shared;
using MedicalConsultation.Helpers;
using MedicalConsultation.Services.Core.Consultation;
using MedicalConsultation.Services.Shared.Enum;

using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace MedicalConsultation.ControlPanel.Controllers
{
    [Authorize(Roles = "Administrator,Patient")]
    public class ConsultationController : BaseController
    {
        private readonly IConsultationRepository _consultationRepository;
        private readonly IPartialViewRender _partialViewRender;
        public ConsultationController(IConsultationRepository consultationRepository, IPartialViewRender partialViewRender)
        {
            _consultationRepository = consultationRepository;
            _partialViewRender = partialViewRender;
        }
        [Authorize(Roles = "Administrator")]
        public IActionResult Index(bool markedAsDone = false)
        {
            var vm = new IndexViewModel("Consultation management", "Med. Consult.")
            {
                ModalViewModel = new ViewModels.ModalViewModel()
                {
                    ModalTitle = "Reply for a consultation"
                }
            };
            ViewData["MarkedAsDone"] = markedAsDone;
            if (markedAsDone)
            {
                ViewData["ConsultationStatus"] = "Already answered";
                vm.AddConsultations(_consultationRepository.GetConsultationsMarkedAsDone());
            }
            else
            {
                ViewData["ConsultationStatus"] = "Waiting for an answer";
                vm.AddConsultations(_consultationRepository.GetConsultationsWaitingToBeAnswered());
            }

            return View(vm);
        }

        [Authorize(Roles = "Patient")]
        public IActionResult PatientConsultations()
        {
            var vm = new IndexViewModel("My consultation", "Med. Consult.")
            {
                ModalViewModel = new ViewModels.ModalViewModel()
                {
                    ModalTitle = "Ask a doctor"
                }
            };
            vm.AddConsultations(_consultationRepository.GetPatientConsultation(User.FindFirstValue(ClaimTypes.NameIdentifier)));

            return View(vm);
        }

        [Authorize(Roles = "Administrator,Patient")]
        public IActionResult MyConsultationDetails(int id)
        {
            var consultationDetails = _consultationRepository.GetConsultation(id);
            if (consultationDetails is null)
            {
                return RedirectToAction("Index", "Home");
            }

            var vm = new MyConsultationDetailsViewModel()
            {
                ConsultationId = consultationDetails.ConsultationId,
                Content = consultationDetails.Content,
                IssuedOn = consultationDetails.IssuedOn,
                MarkedAsDone = consultationDetails.MarkedAsDone,
                PatientId = consultationDetails.PatientId,
                Subject = consultationDetails.Subject,
                Replies = consultationDetails.Replies,
                IsAdmin = User.IsInRole("Administrator")
            };
            if (vm.PatientId == User.FindFirstValue(ClaimTypes.NameIdentifier)
                &&
                !vm.IsAdmin)
            {
                _consultationRepository.MarkConsultationRepliesAsSeen(id);
            }
            return View(vm);
        }

        [Authorize(Roles = "Administrator")]
        public IActionResult GetConsultReplyForm(int id)
        {
            if (IsNotAjaxRequest())
            {
                return NotFound();
            }
            var formVM = new ConsultationReplyFormViewModel()
            {
                AdminID = User.FindFirstValue(ClaimTypes.NameIdentifier),
                ConsultationID = id
            };


            return PartialView(PartialPathProvider.ConsultationReplyForm, formVM);
        }

        [HttpGet]
        [Authorize(Roles = "Administrator")]
        public IActionResult GetConsultReplyIndexDatatable(bool markedAsDone)
        {
            if (IsNotAjaxRequest())
            {
                return NotFound();
            }
            var data = markedAsDone ? _consultationRepository.GetConsultationsMarkedAsDone() : _consultationRepository.GetConsultationsWaitingToBeAnswered();

            var rawPartialViewModel = data.Select(mt => new TableRawViewModel<ConsultationDto>(mt, true, false, false));
            try
            {
                return GetSuccessJsonResult("Updating datatable - successed!", "The datatable has been updated successfully with the new data!", new
                {
                    TableRows = rawPartialViewModel.Select(async row => await _partialViewRender.GetViewAsTextAsync(row, PartialPathProvider.ConsultationTableRow))
                });
            }
            catch
            {
                return GetFailureJsonResult("Saving consultation successed but! :(", "We couldn't refresh the table, please refresh the page!");
            }
        }


        [HttpGet]
        [Authorize(Roles = "Patient")]
        public IActionResult GetIndexDatatable()
        {
            if (IsNotAjaxRequest())
            {
                return NotFound();
            }
            var patientConsultations = _consultationRepository.GetPatientConsultation(User.FindFirstValue(ClaimTypes.NameIdentifier));

            var rawPartialViewModel = patientConsultations.Select(mt => new TableRawViewModel<ConsultationDto>(mt, true, false, false));
            try
            {
                return GetSuccessJsonResult("Updating datatable - successed!", "The datatable has been updated successfully with the new data!", new
                {
                    TableRows = rawPartialViewModel.Select(async row => await _partialViewRender.GetViewAsTextAsync(row, PartialPathProvider.PatientConsultTableRow))
                });
            }
            catch
            {
                return GetFailureJsonResult("Saving consultation successed but! :(", "We couldn't refresh the table, please refresh the page!");
            }
        }

        [Authorize(Roles = "Patient")]
        public IActionResult GetConsultForm(int id)
        {
            if (IsNotAjaxRequest())
            {
                return NotFound();
            }
            var formVM = new ConsultationFormViewModel()
            {
                MarkedAsDone = false,
                PatientId = User.FindFirstValue(ClaimTypes.NameIdentifier)
            };

            if (id == 0)
            {
                return PartialView(PartialPathProvider.ConsultationForm, formVM);
            }

            var entityResult = _consultationRepository.GetConsultation(id);
            if (entityResult is null)
            {
                return GetFailureJsonResult("Doesn't exist", "There's consultation with this ID");
            }

            formVM.ConsultationId = entityResult.ConsultationId;
            formVM.Content = entityResult.Content;
            formVM.IssuedOn = entityResult.IssuedOn;
            formVM.MarkedAsDone = entityResult.MarkedAsDone;
            formVM.PatientId = entityResult.PatientId;
            formVM.HistoricalInformation = entityResult.HistoricalInformation;
            formVM.Subject = entityResult.Subject;

            return PartialView(PartialPathProvider.ConsultationForm, formVM);
        }




        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Patient")]
        public async Task<IActionResult> ConsultAction(ConsultationFormViewModel formVM)
        {
            if (IsNotAjaxRequest())
            {
                return NotFound();
            }

            if (!ModelState.IsValid)
            {
                return GetFailureJsonResult("Form is invalid", "Please fix the form error!");
            }
            var actionOperationType = formVM.ConsultationId == 0 ? ActionOperationType.Create : ActionOperationType.Update;

            var data = new ConsultationFormDto(actionOperationType)
            {
                ConsultationId = formVM.ConsultationId,
                Content = formVM.Content,
                IssuedOn = formVM.IssuedOn,
                MarkedAsDone = formVM.MarkedAsDone,
                PatientId = formVM.PatientId,
                HistoricalInformation = formVM.HistoricalInformation,
                Subject = formVM.Subject
            };

            var result = _consultationRepository.Action(data);

            if (result is null)
            {

                return GetFailureJsonResult("Saving consultation - failed :(", $"Could not save the consultation try again.");
            }

            var returnedData = _consultationRepository.GetConsultation(result.ConsultationId);
            var rawPartialViewModel = new TableRawViewModel<ConsultationDto>(returnedData, true, false, false);

            try
            {
                return GetSuccessJsonResult("Saving consultation - success! ", $"Your consultation <b>{data.Subject} </b> has been saved successfully!", new
                {
                    TableRaw = await _partialViewRender.GetViewAsTextAsync(rawPartialViewModel, PartialPathProvider.PatientConsultTableRow),
                    Data = returnedData
                });
            }
            catch
            {
                return GetFailureJsonResult("Saving consultation successed but! :(", "We couldn't refresh the table, please refresh the page!");
            }
        }



        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrator")]
        public async Task<IActionResult> ConsultReplyAction(ConsultationReplyFormViewModel formVM)
        {
            if (IsNotAjaxRequest())
            {
                return NotFound();
            }

            if (!ModelState.IsValid)
            {
                return GetFailureJsonResult("Form is invalid", "Please fix the form error!");
            }
            var actionOperationType = formVM.ConsultationReplyID == 0 ? ActionOperationType.Create : ActionOperationType.Update;

            var data = new ConsultationReplyFormDto(actionOperationType)
            {
                AdminID = formVM.AdminID,
                ConsultationID = formVM.ConsultationID,
                Content = formVM.Content,
                IssuedDate = formVM.IssueDate,
                Subject = formVM.Subject,
                MarkedAsSeen = false,
            };

            var result = _consultationRepository.Reply(data);
            if (result is null)
            {

                return GetFailureJsonResult("Saving consultation reply - failed :(", $"Could not save the consultation reply try again.");
            }

            var returnedData = _consultationRepository.GetConsultation(result.ConsultationID);
            var rawPartialViewModel = new TableRawViewModel<ConsultationDto>(returnedData, true, true, true);

            try
            {
                return GetSuccessJsonResult("Saving consultation reply - success! ", $"Your consultation reply <b>{data.Subject} </b> has been saved successfully!", new
                {
                    TableRaw = await _partialViewRender.GetViewAsTextAsync(rawPartialViewModel, PartialPathProvider.ConsultationTableRow),
                    Data = returnedData
                });
            }
            catch
            {
                return GetFailureJsonResult("Saving consultation successed but! :(", "We couldn't refresh the table, please refresh the page!");
            }
        }
    }
}
