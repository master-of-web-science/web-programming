﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Microsoft.AspNetCore.Mvc;

namespace MedicalConsultation.ControlPanel.Controllers
{

    public class BaseController : Controller
    {

        protected bool IsAjaxRequest()
        {
            if (Request.Headers is null)
            {
                throw new NullReferenceException("The request's Headers property is null");
            }

            if (Request.Headers.ContainsKey("X-Requested-With"))
            {
                if (Request.Headers["X-Requested-With"] == "XMLHttpRequest")
                {
                    return true;
                }
            }

            return false;
        }

        protected bool IsNotAjaxRequest() => !(IsAjaxRequest());


        protected virtual JsonResult GetSuccessJsonResult<TData>(string title, string message, TData data = null)
            where TData : class
        {

            ThrowExceptionIfNotAjax();

            HttpContext.Response.StatusCode = Ok().StatusCode;
            return Json(new
            {
                Title = title,
                Message = message,
                Data = data
            });
        }
        protected virtual JsonResult GetFailureJsonResult<TData>(string title, string message, TData data = null)
           where TData : class
        {
            ThrowExceptionIfNotAjax();

            HttpContext.Response.StatusCode = BadRequest().StatusCode;
            return Json(new
            {
                Title = title,
                Message = message,
                Data = data
            });
        }

        protected virtual JsonResult GetSuccessJsonResult(string title, string message)
        {
            ThrowExceptionIfNotAjax();

            HttpContext.Response.StatusCode = Ok().StatusCode;
            return Json(new
            {
                Title = title,
                Message = message
            });
        }
        protected virtual JsonResult GetFailureJsonResult(string title, string message)
        {
            ThrowExceptionIfNotAjax();

            HttpContext.Response.StatusCode = BadRequest().StatusCode;
            return Json(new
            {
                Title = title,
                Message = message
            });
        }

        private void ThrowExceptionIfNotAjax()
        {
            if (IsNotAjaxRequest())
            {
                throw new InvalidOperationException("You are trying to return Json result even though it's not an ajax request");
            }
        }
    }
}
