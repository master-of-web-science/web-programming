﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using MedicalConsultation.ControlPanel.ViewModels;
using Microsoft.AspNetCore.Authorization;
using MedicalConsultation.Services.Core.Consultation;
using MedicalConsultation.Services.Core.Patient;
using MedicalConsultation.Services.Core.MedicalTopic;
using System.Security.Claims;
using MedicalConsultation.ControlPanel.ViewModels.Account;

namespace MedicalConsultation.ControlPanel.Controllers
{
    [Authorize(Roles = "Administrator,Patient")]
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly IAccountRepository _accountRepository;
        private readonly IMedicalTopicRepository _medicalTopicRepository;
        private readonly IConsultationRepository _consultationRepository;
        public HomeController(ILogger<HomeController> logger, IConsultationRepository consultationRepository, IAccountRepository accountRepository, IMedicalTopicRepository medicalTopicRepository)
        {
            _logger = logger;
            _accountRepository = accountRepository;
            _medicalTopicRepository = medicalTopicRepository;
            _consultationRepository = consultationRepository;
        }

        [AllowAnonymous]
        public async Task<IActionResult> Index()
        {
            if (!User.Identity.IsAuthenticated)
            {
                return RedirectToAction(nameof(PatientIndex));
            }
            var vm = new ViewModels.IndexViewModel("Dashboard", "Med. Consult.")
            {
                IsAdmin = User.IsInRole("Administrator")
            };

            vm.SetMedicalTopics(_medicalTopicRepository.GetMedicalTopics());
            var accounts = await _accountRepository.GetAccounts();
            vm.SetAccounts(accounts);

            vm.SetConsultants(_consultationRepository.GetConsultationsWaitingToBeAnswered());

            if (!vm.IsAdmin && User.Identity.IsAuthenticated)
            {
                return RedirectToAction(nameof(PatientIndex));
            }
            return View(vm);
        }

        [AllowAnonymous]
        public IActionResult About()
        {
            var vm = new AboutViewModel()
            {
                IsAdmin = User.IsInRole("Administrator")
            };
            return View(vm);
        }
        [Authorize(Roles = "Administrator")]
        public async Task<IActionResult> AccountInformation(string userId)
        {
            var x = await _accountRepository.GetAccount(userId);
            
            var vm = new MyAccountFormViewModel("My account", "Med. Consult.")
            {
                BirthDate = x.BirthDate,
                Email = x.Email,
                FirstName = x.FirstName,
                LastName = x.LastName,
                Password = x.Password,
                PhoneNumber = x.PhoneNumber,
                Role = x.Role,
                Sex = x.Sex,
                UserID = x.UserID,
                Username = x.Username
            };

            return View(vm);
        }
        [AllowAnonymous]
        public IActionResult PatientIndex()
        {
            var vm = new PatientIndexViewModel("Dashboard", "Med. Consult.");
            var consult = _consultationRepository.GetPatientConsultation(User.FindFirstValue(ClaimTypes.NameIdentifier));
            vm.HasReplyNotSeen = consult.Any(x => x.Replies.Any(x => !x.MarkedAsSeen));
            vm.AddMedicalTopics(_medicalTopicRepository.GetMedicalTopics().OrderByDescending(x => x.NumberOfVisits));
            return View(vm);
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        [Authorize(Roles = "Administrator,Patient")]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        [AllowAnonymous]
        [Authorize(Roles = "Administrator,Patient")]
        public IActionResult PageNotFound()
        {
            return View(new BaseViewModel("Page was not found", "Med. Consult."));
        }

        [AllowAnonymous]
        public IActionResult MedicalExpertSys()
        {
            return View(new BaseViewModel("Medical Expert System", "Med. Consult."));
        }
    }
}
