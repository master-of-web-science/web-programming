﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Security.Cryptography.X509Certificates;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using MedicalConsultation.ControlPanel.Helpers.Renders;
using MedicalConsultation.ControlPanel.ViewModels.Account;
using MedicalConsultation.ControlPanel.ViewModels.Shared;
using MedicalConsultation.Helpers;
using MedicalConsultation.Services.Core.Account;
using MedicalConsultation.Services.Core.Patient;
using MedicalConsultation.Services.Shared.Enum;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace MedicalConsultation.ControlPanel.Controllers
{
    [Authorize(Roles = "Administrator")]
    public class AccountController : BaseController
    {
        private readonly IAccountRepository _accountRepository;
        private readonly IPartialViewRender _partialViewRender;
        public AccountController(IAccountRepository accountRepository, IPartialViewRender partialViewRender)
        {
            _accountRepository = accountRepository;
            _partialViewRender = partialViewRender;
        }

        public async Task<IActionResult> Index()
        {
            var vm = new IndexViewModel("Accounts management")
            {
                ModalViewModel = new ViewModels.ModalViewModel()
                {
                    ModalTitle = "Create new account"
                }
            };
            var ac = await _accountRepository.GetAccounts();
            vm.AddAccounts(ac.Where(user => user.UserID != User.FindFirstValue(ClaimTypes.NameIdentifier)));
            return View(vm);
        }

        [HttpGet]
        public async Task<IActionResult> GetAccountRoles()
        {
            if (IsNotAjaxRequest())
            {
                return NotFound();
            }

            var roles = await _accountRepository.GetAccountRoles();
            return Json(roles);
        }

        [HttpGet]
        public async Task<IActionResult> GetAccountForm(string id)
        {
            if (IsNotAjaxRequest())
            {
                return NotFound();
            }
            var formVM = new AccountFormViewModel();

            if (string.IsNullOrEmpty(id))
            {
                return PartialView(PartialPathProvider.AccountForm, formVM);
            }

            var entityResult = await _accountRepository.GetAccount(id);
            if (entityResult is null)
            {
                return GetFailureJsonResult("Doesn't exist", "There's no account with this ID");
            }

            formVM.BirthDate = entityResult.BirthDate;
            formVM.Email = entityResult.Email;
            formVM.FirstName = entityResult.FirstName;
            formVM.LastName = entityResult.LastName;
            formVM.Password = entityResult.Password;
            formVM.PhoneNumber = entityResult.PhoneNumber;
            formVM.Role = entityResult.Role;
            formVM.Sex = entityResult.Sex;
            formVM.UserID = entityResult.UserID;
            formVM.Username = entityResult.Username;

            var roles = await _accountRepository.GetAccountRoles();
            formVM.Roles = roles.Select( x => new SelectListItem()
            {
                Text = x.RoleName,
                Value = x.RoleId
            });

            return PartialView(PartialPathProvider.AccountForm, formVM);
        }


        [HttpGet]
        public async Task<IActionResult> GetIndexDatatable()
        {
            if (IsNotAjaxRequest())
            {
                return NotFound();
            }
            var accounts = await _accountRepository.GetAccounts();
            var rawPartialViewModel = accounts.Select(mt => new TableRawViewModel<AccountDto>(mt, true, true, true));
            try
            {
                return GetSuccessJsonResult("Updating datatable - successed!", "The datatable has been updated successfully with the new data!", new
                {
                    TableRows = rawPartialViewModel.Select(async row => await _partialViewRender.GetViewAsTextAsync(row, PartialPathProvider.AccountTableRow))
                });
            }
            catch
            {
                return GetFailureJsonResult("Saving account successed but! :(", "We couldn't refresh the table, please refresh the page!");
            }
        }



        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Action(AccountFormViewModel formVM)
        {
            if (IsNotAjaxRequest())
            {
                return NotFound();
            }

            if (!ModelState.IsValid)
            {
                return GetFailureJsonResult("Form is invalid", "Please fix the form error!");
            }
            var actionOperationType = string.IsNullOrEmpty(formVM.UserID) ? ActionOperationType.Create : ActionOperationType.Update;

            var data = new AccountFormDto(actionOperationType)
            {
                Username = formVM.Username,
                BirthDate = formVM.BirthDate,
                FirstName = formVM.FirstName,
                LastName = formVM.LastName,
                PhoneNumber = formVM.PhoneNumber,
                Sex = formVM.Sex,
                Email = formVM.Email,
                Role = formVM.Role,
                Password = formVM.Password,
                UserID = formVM.UserID
            };

            var result = await _accountRepository.Action(data);

            if (result is null)
            {

                return GetFailureJsonResult("Saving account - failed :(", $"Could not save the account try again.");
            }

            var returnedData = await _accountRepository.GetAccount(result.UserID);
            var rawPartialViewModel = new TableRawViewModel<AccountDto>(returnedData, true, true, true);

            try
            {
                return GetSuccessJsonResult("Saving account - success! ", $"Your account <b>{data.Username} </b> has been saved successfully!", new
                {
                    TableRaw = await _partialViewRender.GetViewAsTextAsync(rawPartialViewModel, PartialPathProvider.AccountTableRow),
                    Data = returnedData
                });
            }
            catch
            {
                return GetFailureJsonResult("Saving account successed but! :(", "We couldn't refresh the table, please refresh the page!");
            }
        }


        [HttpDelete]
        public async Task<IActionResult> Remove(string id)
        {
            try
            {
                if (IsNotAjaxRequest())
                {
                    return NotFound();
                }
                if (string.IsNullOrEmpty(id))
                {
                    return GetFailureJsonResult("Remove account - failed :(", "Your id is null.");
                }
                var removeResult = await _accountRepository.Remove(id);
                if (!removeResult)
                {
                    return GetFailureJsonResult("Remove account - failed :(", "");
                }
            }
            catch (Exception)
            {
                return GetFailureJsonResult("Remove account - failed :(", "Your id is null.");

            }
            return GetSuccessJsonResult("Remove account - successed!", "The account has been deleted successfully!");
        }


        [AllowAnonymous]
        [HttpGet]
        public async Task<JsonResult> VerifyUsernameCorrectness(string username)
        {
            if ((string.IsNullOrEmpty(username) || string.IsNullOrWhiteSpace(username)))
            {
                return Json(new
                {
                    UsernameIsValid = false,
                    Errors = "Please insert a username."
                });
            }

            var usernameCheckResult = await _accountRepository.CheckUsernameCorrectness(username);
            return Json(new
            {
                UsernameIsValid = usernameCheckResult.Item1,
                Errors = string.Join("<br />", usernameCheckResult.Item2.Select(errorMessage => errorMessage))
            });
        }

        [HttpGet]
        [AllowAnonymous]
        public async Task<JsonResult> VerifyPasswordCorrectness(string password, bool alreadyHasPassword)
        {
            if ((string.IsNullOrEmpty(password) || string.IsNullOrWhiteSpace(password)) && !alreadyHasPassword)
            {
                return Json(new
                {
                    PasswordIsValid = false,
                    Errors = "Please insert a password."
                });
            }
            else if ((string.IsNullOrEmpty(password) || string.IsNullOrWhiteSpace(password)) && alreadyHasPassword)
            {
                return Json(new
                {
                    PasswordIsValid = true,
                    Errors = "Please insert a password."
                });
            }

            var passwordCheckResult = await _accountRepository.CheckPasswordCorrectness(password);
            return Json(new
            {
                PasswordIsValid = passwordCheckResult.Item1,
                Errors = string.Join("<br />", passwordCheckResult.Item2.Select(errorMessage => errorMessage))
            });
        }

        [HttpGet]
        [AllowAnonymous]
        public async Task<JsonResult> VerifyEmailCorrectness(string email)
        {
            if ((string.IsNullOrEmpty(email) || string.IsNullOrWhiteSpace(email)))
            {
                return Json(new
                {
                    EmailIsValid = false,
                    Errors = "Please insert a email."
                });
            }
            var regex = new Regex(@"^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$", RegexOptions.Compiled);
            if (!regex.IsMatch(email))
            {
                return Json(new
                {
                    EmailIsValid = false,
                    Errors = "Please insert a valid email."
                });
            }

            var emailCheckResult = await _accountRepository.CheckEmailCorrectness(email);
            return Json(new
            {
                EmailIsValid = emailCheckResult.Item1,
                Errors = string.Join("<br />", emailCheckResult.Item2.Select(errorMessage => errorMessage))
            });
        }
    }
}
